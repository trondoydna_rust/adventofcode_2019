#[derive(Debug)]
enum Operation {
    Add,
    Multiply,
    Input,
    Output,
    JumpIfTrue,
    JumpIfFalse,
    LessThan,
    Equals,
    Terminate,
}

impl Operation {
    fn from_code(code: usize) -> Self {
        match code {
            1 => Operation::Add,
            2 => Operation::Multiply,
            3 => Operation::Input,
            4 => Operation::Output,
            5 => Operation::JumpIfTrue,
            6 => Operation::JumpIfFalse,
            7 => Operation::LessThan,
            8 => Operation::Equals,
            99 => Operation::Terminate,
            n => panic!("Unknown opcode {}", n),
        }
    }
}

fn main() {
    task1();
    task2();
}

/// 13346482
fn task1() {
    let initial: Vec<isize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<isize>().unwrap())
        .collect();

    let res = run(&initial, 1);

    println!("Task 1 answer: {:?}", res);
}

/// 12111395
fn task2() {
    let initial: Vec<isize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<isize>().unwrap())
        .collect();

    let res = run(&initial, 5);

    println!("Task 2 answer: {:?}", res);
}

fn run(initial: &Vec<isize>, input: isize) -> Vec<isize> {
    let mut ints = initial.clone();

    let mut output = Vec::new();
    let mut i = 0;

    loop {
        dbg!(i);

        let mut opcode = ints[i];
        let mut immediate1 = false;
        let mut immediate2 = false;
        let mut immediate3 = false;
        if opcode > 10_000 {
            immediate3 = true;
            opcode -= 10_000;
        }
        if opcode > 1_000 {
            immediate2 = true;
            opcode -= 1_000;
        }
        if opcode > 100 {
            immediate1 = true;
            opcode -= 100;
        }
        dbg!(ints[i], opcode, immediate1, immediate2, immediate3);

        let op = Operation::from_code(opcode as usize);

        match op {
            Operation::Add => {
                let input1 = read_at(&ints, i + 1, immediate1);
                let input2 = read_at(&ints, i + 2, immediate2);
                let output_index = ints[i + 3];
                let res = input1 + input2;
                dbg!(input1, input2, output_index, res);

                ints[output_index as usize] = res;
                i += 4
            }
            Operation::Multiply => {
                let input1 = read_at(&ints, i + 1, immediate1);
                let input2 = read_at(&ints, i + 2, immediate2);
                let output_index = ints[i + 3];
                let res = input1 * input2;
                dbg!(input1, input2, output_index, res);

                ints[output_index as usize] = res;
                i += 4
            }
            Operation::Input => {
                let store_index = ints[i + 1];
                dbg!(store_index);

                ints[store_index as usize] = input;
                i += 2;
            }
            Operation::Output => {
                let out = read_at(&ints, i + 1, immediate1);

                dbg!(out);

                output.push(out);
                i += 2;
            }
            Operation::JumpIfTrue => {
                let condition = read_at(&ints, i + 1, immediate1);
                let new_pointer = read_at(&ints, i + 2, immediate2);

                if condition != 0 {
                    i = new_pointer as usize;
                } else {
                    i += 3;
                }
            }
            Operation::JumpIfFalse => {
                let condition = read_at(&ints, i + 1, immediate1);
                let new_pointer = read_at(&ints, i + 2, immediate2);

                if condition == 0 {
                    i = new_pointer as usize;
                } else {
                    i += 3;
                }
            }
            Operation::LessThan => {
                let param1 = read_at(&ints, i + 1, immediate1);
                let param2 = read_at(&ints, i + 2, immediate2);
                let output_index = ints[i + 3] as usize;

                if param1 < param2 {
                    ints[output_index] = 1;
                } else {
                    ints[output_index] = 0;
                }
                i += 4;
            }
            Operation::Equals => {
                let param1 = read_at(&ints, i + 1, immediate1);
                let param2 = read_at(&ints, i + 2, immediate2);
                let output_index = ints[i + 3] as usize;

                if param1 == param2 {
                    ints[output_index] = 1;
                } else {
                    ints[output_index] = 0;
                }
                i += 4;
            }
            Operation::Terminate => {
                break;
            }
        }

        println!();
    }

    output
}

fn read_at(ints: &Vec<isize>, i: usize, immediate1: bool) -> isize {
    if immediate1 {
        dbg!(ints[i])
    } else {
        dbg!(ints[dbg!(ints[i]) as usize])
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_in_out() {
        assert_eq!(run(&vec![3, 0, 4, 0, 99], 1), vec![1]);
    }

    #[test]
    fn test_param_mode() {
        assert_eq!(run(&vec![1002, 4, 3, 4, 33], 1), vec![]);
    }

    #[test]
    fn test_param_mode_2() {
        assert_eq!(run(&vec![1101, 100, -1, 4, 0], 1), vec![]);
    }

    #[test]
    fn is_equal_to() {
        assert_eq!(run(&vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 8), vec![1]);
        assert_eq!(run(&vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 88), vec![0]);

        assert_eq!(run(&vec![3, 3, 1108, -1, 8, 3, 4, 3, 99], 8), vec![1]);
        assert_eq!(run(&vec![3, 3, 1108, -1, 8, 3, 4, 3, 99], 88), vec![0]);
    }

    #[test]
    fn is_less_than() {
        assert_eq!(run(&vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 0), vec![1]);
        assert_eq!(run(&vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 8), vec![0]);

        assert_eq!(run(&vec![3, 3, 1107, -1, 8, 3, 4, 3, 99], 0), vec![1]);
        assert_eq!(run(&vec![3, 3, 1107, -1, 8, 3, 4, 3, 99], 8), vec![0]);
    }

    #[test]
    fn looping() {
        assert_eq!(
            run(
                &vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9],
                0
            ),
            vec![0]
        );
        assert_eq!(
            run(
                &vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9],
                111
            ),
            vec![1]
        );

        assert_eq!(
            run(&vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 0),
            vec![0]
        );
        assert_eq!(
            run(&vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 111),
            vec![1]
        );
    }

    #[test]
    fn example() {
        let program = vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99];

        assert_eq!(run(&program, 1), vec![999]);
        assert_eq!(run(&program, 8), vec![1000]);
        assert_eq!(run(&program, 17), vec![1001]);
    }
}
