#[derive(Clone, Copy, Debug)]
enum ParamMode {
    Position,
    Immediate,
    Relative,
}

#[derive(Clone, Copy, Debug)]
struct Input {
    value: isize,
}

#[derive(Debug)]
struct Output {
    location: usize,
}

#[derive(Debug)]
enum Operation {
    Add(Input, Input, Output),
    Multiply(Input, Input, Output),
    Input(Output),
    Output(Input),
    JumpIfTrue(Input, Input),
    JumpIfFalse(Input, Input),
    LessThan(Input, Input, Output),
    Equals(Input, Input, Output),
    AdjustRelativeBase(Input),
    Terminate,
}

impl Operation {
    fn new(code: usize, run: &mut RunningProgramState) -> Self {
        match code {
            1 => Operation::Add(run.input(1), run.input(2), run.output(3)),
            2 => Operation::Multiply(run.input(1), run.input(2), run.output(3)),
            3 => Operation::Input(run.output(1)),
            4 => Operation::Output(run.input(1)),
            5 => Operation::JumpIfTrue(run.input(1), run.input(2)),
            6 => Operation::JumpIfFalse(run.input(1), run.input(2)),
            7 => Operation::LessThan(run.input(1), run.input(2), run.output(3)),
            8 => Operation::Equals(run.input(1), run.input(2), run.output(3)),
            9 => Operation::AdjustRelativeBase(run.input(1)),
            99 => Operation::Terminate,
            n => panic!("Unknown opcode {}", n),
        }
    }

    fn execute(&mut self, run: &mut RunningProgramState) -> Result<Option<isize>, ()> {
        match self {
            Operation::Add(i1, i2, o) => {
                run.write_to(o, i1.value + i2.value);

                Ok(None)
            }
            Operation::Multiply(i1, i2, o) => {
                run.write_to(o, i1.value * i2.value);

                Ok(None)
            }
            Operation::Input(o) => {
                run.write_to(o, run.program.input);

                Ok(None)
            }
            Operation::Output(i) => Ok(Some(i.value)),
            Operation::JumpIfTrue(_, _) | Operation::JumpIfFalse(_, _) => Ok(None),
            Operation::LessThan(i1, i2, o) => {
                if i1.value < i2.value {
                    run.write_to(o, 1);
                } else {
                    run.write_to(o, 0);
                }
                Ok(None)
            }
            Operation::Equals(i1, i2, o) => {
                if i1.value == i2.value {
                    run.write_to(o, 1);
                } else {
                    run.write_to(o, 0);
                }
                Ok(None)
            }
            Operation::AdjustRelativeBase(i) => {
                run.relative_base += i.value;
                Ok(None)
            }
            Operation::Terminate => Err(()),
        }
    }

    fn next_instruction_index(&self, current: usize) -> usize {
        match self {
            Operation::Terminate => 0,
            Operation::Input(_) | Operation::Output(_) | Operation::AdjustRelativeBase(_) => {
                current + 2
            }
            Operation::Add(_, _, _)
            | Operation::Multiply(_, _, _)
            | Operation::LessThan(_, _, _)
            | Operation::Equals(_, _, _) => current + 4,
            Operation::JumpIfTrue(i1, i2) => {
                if i1.value != 0 {
                    i2.value as usize
                } else {
                    current + 3
                }
            }
            Operation::JumpIfFalse(i1, i2) => {
                if i1.value == 0 {
                    i2.value as usize
                } else {
                    current + 3
                }
            }
        }
    }
}

#[derive(Clone)]
struct Program {
    intcodes: Vec<isize>,
    input: isize,
}

impl Program {
    fn new(intcodes: Vec<isize>, input: isize) -> Self {
        Self { intcodes, input }
    }

    fn run(self) -> Vec<isize> {
        self.into_iter().flatten().collect()
    }
}

impl<'a> IntoIterator for Program {
    type Item = Option<isize>;
    type IntoIter = RunningProgramState;

    fn into_iter(self) -> Self::IntoIter {
        RunningProgramState {
            program: self,
            instruction_index: 0,
            relative_base: 0,
        }
    }
}

struct RunningProgramState {
    program: Program,
    instruction_index: usize,
    relative_base: isize,
}

impl RunningProgramState {
    fn input(&mut self, offset: usize) -> Input {
        let mode = self.param_mode(offset);
        let i = self.instruction_index + offset;
        let val = self.read_from(i);

        let value = match mode {
            ParamMode::Position => self.read_from(val as usize),
            ParamMode::Immediate => val,
            ParamMode::Relative => self.read_from((val + self.relative_base) as usize),
        };

        Input { value }
    }

    fn read_from(&mut self, i: usize) -> isize {
        if i > self.program.intcodes.len() {
            self.program.intcodes.resize(i + 1, 0);
            0
        } else {
            self.program.intcodes[i]
        }
    }

    fn output(&self, offset: usize) -> Output {
        let mode = self.param_mode(offset);

        let location = self.program.intcodes[self.instruction_index + offset] as usize;
        let location = match mode {
            ParamMode::Position => location,
            ParamMode::Relative => (location as isize + self.relative_base) as usize,
            ParamMode::Immediate => panic!("Cannot write with mode immediate"),
        };

        Output { location }
    }

    fn write_to(&mut self, output: &Output, value: isize) {
        let i = output.location;
        if i >= self.program.intcodes.len() {
            self.program.intcodes.resize(i + 1, 0);
        }
        self.program.intcodes[i] = value;
    }

    fn param_mode(&self, offset: usize) -> ParamMode {
        let mut opcode = self.program.intcodes[self.instruction_index] as usize;

        let mut mode = ParamMode::Position;

        if opcode > 20_000 {
            mode = ParamMode::Relative;
            opcode -= 20_000;
        }
        if opcode > 10_000 {
            mode = ParamMode::Immediate;
            opcode -= 10_000;
        }
        if offset == 3 {
            return mode;
        }

        let mut mode = ParamMode::Position;
        if opcode > 2_000 {
            mode = ParamMode::Relative;
            opcode -= 2_000;
        }
        if opcode > 1_000 {
            mode = ParamMode::Immediate;
            opcode -= 1_000;
        }
        if offset == 2 {
            return mode;
        }

        if opcode > 200 {
            ParamMode::Relative
        } else if opcode > 100 {
            ParamMode::Immediate
        } else {
            ParamMode::Position
        }
    }

    fn opcode(&self) -> usize {
        self.program.intcodes[self.instruction_index] as usize % 100
    }
}

impl Iterator for RunningProgramState {
    type Item = Option<isize>;

    fn next(&mut self) -> Option<Self::Item> {
        let opcode = self.opcode();

        let mut operation = Operation::new(opcode, self);

        let next_instruction_index = operation.next_instruction_index(self.instruction_index);
        let res = operation.execute(self);

        self.instruction_index = next_instruction_index;

        match res {
            Ok(o) => Some(o),
            Err(()) => None,
        }
    }
}

fn main() {
    task1();
    task2();
}

/// 3533056970
fn task1() {
    let intcodes: Vec<isize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<isize>().unwrap())
        .collect();

    let program = Program::new(intcodes, 1);

    println!("Task 1 answer is {:?}", program.run());
}

/// 72852
fn task2() {
    let intcodes: Vec<isize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<isize>().unwrap())
        .collect();

    let program = Program::new(intcodes, 2);

    println!("Task 2 answer is {:?}", program.run());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_in_out() {
        assert_eq!(Program::new(vec![3, 0, 4, 0, 99], 1).run(), vec![1]);
    }

    #[test]
    fn test_immediate() {
        assert_eq!(Program::new(vec![1002, 4, 3, 4, 33], 1).run(), vec![]);
    }

    #[test]
    fn test_immediate_2() {
        assert_eq!(Program::new(vec![1101, 100, -1, 4, 0], 1).run(), vec![]);
    }

    #[test]
    fn is_equal_to() {
        assert_eq!(
            Program::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 8).run(),
            vec![1]
        );
        assert_eq!(
            Program::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 88).run(),
            vec![0]
        );

        assert_eq!(
            Program::new(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99], 8).run(),
            vec![1]
        );
        assert_eq!(
            Program::new(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99], 88).run(),
            vec![0]
        );
    }

    #[test]
    fn is_less_than() {
        assert_eq!(
            Program::new(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 0).run(),
            vec![1]
        );
        assert_eq!(
            Program::new(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 8).run(),
            vec![0]
        );

        assert_eq!(
            Program::new(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99], 0).run(),
            vec![1]
        );
        assert_eq!(
            Program::new(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99], 8).run(),
            vec![0]
        );
    }

    #[test]
    fn looping() {
        assert_eq!(
            Program::new(
                vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9],
                0
            )
                .run(),
            vec![0]
        );
        assert_eq!(
            Program::new(
                vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9],
                111
            )
                .run(),
            vec![1]
        );

        assert_eq!(
            Program::new(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 0).run(),
            vec![0]
        );
        assert_eq!(
            Program::new(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 111).run(),
            vec![1]
        );
    }

    #[test]
    fn example_day5() {
        let mut program = Program::new(
            vec![
                3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36,
                98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000,
                1, 20, 4, 20, 1105, 1, 46, 98, 99,
            ],
            1,
        );
        assert_eq!(program.clone().run(), vec![999]);

        program.input = 8;
        assert_eq!(program.clone().run(), vec![1000]);

        program.input = 17;
        assert_eq!(program.run(), vec![1001]);
    }

    #[test]
    fn copy_self() {
        assert_eq!(
            Program::new(
                vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99],
                111
            )
                .run(),
            vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
        );
    }

    #[test]
    fn output_middle_number() {
        assert_eq!(
            Program::new(vec![104, 1125899906842624, 99], 111).run(),
            vec![1125899906842624]
        );
    }
}
