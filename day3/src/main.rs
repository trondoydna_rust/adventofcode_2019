use std::str::FromStr;
use std::time::Duration;

#[derive(Clone, Copy, Debug)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

#[derive(Clone, Copy, Debug)]
struct Section {
    direction: Direction,
    distance: u32,
}

#[derive(Debug)]
struct Wire {
    sections: Vec<Section>,
}

#[derive(Clone, Copy, Default, Hash, PartialEq, Eq, Debug)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Clone, Copy, Debug)]
struct PointSection {
    section: Section,
    start: Point,
    end: Point,
    steps_to_start: u32,
}

impl Direction {
    fn is_vertical(&self) -> bool {
        match &self {
            Direction::Up | Direction::Down => true,
            _ => false,
        }
    }
}

impl Wire {
    fn to_point_sections(&self) -> Vec<PointSection> {
        let mut point_sections = Vec::new();
        let mut previous_point = Point { x: 0, y: 0 };
        let mut previous_section: Option<PointSection> = None;

        for section in &self.sections {
            let next_point =
                previous_point.move_in_direction(&section.direction, section.distance as i32);

            let point_section = PointSection {
                section: *section,
                start: previous_point,
                end: next_point,
                steps_to_start: match previous_section {
                    None => 0,
                    Some(s) => s.steps_to_start + s.section.distance,
                },
            };

            dbg!(&point_section);

            point_sections.push(point_section);
            previous_section = Some(point_section);
            previous_point = next_point;
        }

        point_sections
    }
}

impl Point {
    fn move_in_direction(&self, direction: &Direction, distance: i32) -> Point {
        match direction {
            Direction::Up => Self {
                x: self.x,
                y: self.y + distance,
            },
            Direction::Right => Self {
                x: self.x + distance,
                y: self.y,
            },
            Direction::Down => Self {
                x: self.x,
                y: self.y - distance,
            },
            Direction::Left => Self {
                x: self.x - distance,
                y: self.y,
            },
        }
    }

    fn manhattan_distance_from_default(&self) -> i32 {
        dbg!(self.x.abs() + self.y.abs())
    }

    fn distance(&self, other: &Point) -> u32 {
        (self.x - other.x).abs() as u32 + (self.y - other.y).abs() as u32
    }
}

impl PointSection {
    fn overlaps(&self, other: &PointSection) -> Option<Point> {
        let vertical = self.section.direction.is_vertical();

        if vertical == other.section.direction.is_vertical() {
            // parallel
            None
        } else {
            let (other_low, other_high) = match other.section.direction {
                Direction::Up | Direction::Right => (other.start, other.end),
                Direction::Down | Direction::Left => (other.end, other.start),
            };

            match self.section.direction {
                Direction::Up
                    if self.start.x > other_low.x
                        && self.end.x < other_high.x
                        && self.start.y < other_low.y
                        && self.end.y > other_high.y =>
                {
                    Some(Point {
                        x: self.start.x,
                        y: other_low.y,
                    })
                }
                Direction::Right
                    if self.start.x < other_low.x
                        && self.end.x > other_high.x
                        && self.start.y > other_low.y
                        && self.end.y < other_high.y =>
                {
                    Some(Point {
                        x: other_low.x,
                        y: self.start.y,
                    })
                }
                Direction::Down
                    if self.start.x > other_low.x
                        && self.end.x < other_high.x
                        && self.start.y > other_low.y
                        && self.end.y < other_high.y =>
                {
                    Some(Point {
                        x: self.start.x,
                        y: other_low.y,
                    })
                }
                Direction::Left
                    if self.start.x > other_low.x
                        && self.end.x < other_high.x
                        && self.start.y > other_low.y
                        && self.end.y < other_high.y =>
                {
                    Some(Point {
                        x: other_low.x,
                        y: self.start.y,
                    })
                }
                _ => None,
            }
        }
    }
}

fn main() {
    task1();
    task2();
}

/// 557
fn task1() {
    let wires = load_input();
    dbg!("Loaded input");
    let point_sections = (wires.0.to_point_sections(), wires.1.to_point_sections());
    dbg!(
        "Converted to point sections",
        &point_sections.0.len() + point_sections.1.len()
    );
    let overlapping = find_overlapping(&point_sections);
    dbg!("Found overlapping points", &overlapping.len());
    let shortest_distance = overlapping
        .iter()
        .map(|(point, _)| point.manhattan_distance_from_default())
        .fold(
            i32::max_value(),
            |lowest, next| {
                if lowest < next {
                    lowest
                } else {
                    next
                }
            },
        );

    println!("Task 1 answer: {}", shortest_distance);
}

fn task2() {
    let wires = load_input();
    dbg!("Loaded input");
    let point_sections = (wires.0.to_point_sections(), wires.1.to_point_sections());
    dbg!(
        "Converted to point sections",
        &point_sections.0.len() + point_sections.1.len()
    );
    let overlapping = find_overlapping(&point_sections);
    dbg!("Found overlapping points", &overlapping.len());
    let shortest_distance = overlapping
        .iter()
        .map(|(_, sum_steps)| *sum_steps)
        .fold(
            u32::max_value(),
            |lowest, next| {
                if lowest < next {
                    lowest
                } else {
                    next
                }
            },
        );

    println!("Task 2 answer: {}", shortest_distance);
}

fn find_overlapping(point_sections: &(Vec<PointSection>, Vec<PointSection>)) -> Vec<(Point, u32)> {
    let mut overlapping = Vec::new();

    for point_section0 in &point_sections.0 {
        for point_section1 in &point_sections.1 {
            if let Some(point) = point_section0.overlaps(point_section1) {
                let sum_steps = point_section0.steps_to_start
                    + point_section0.start.distance(&point)
                    + point_section1.steps_to_start
                    + point_section1.start.distance(&point);
                overlapping.push((point, sum_steps))
            }
        }
    }

    overlapping
}

fn load_input() -> (Wire, Wire) {
    let wires = std::fs::read_to_string("input");

    if let Err(e) = wires {
        eprintln!("{}", e);
        std::process::exit(1);
    }

    let wires: Vec<Wire> = wires
        .unwrap()
        .split_whitespace()
        .map(|wire| wire.parse())
        .map(|res| {
            if let Err(e) = res {
                eprintln!("{}", e);
                std::process::exit(1);
            }

            res.unwrap()
        })
        .collect();

    let mut wires = wires.into_iter();

    (wires.next().unwrap(), wires.next().unwrap())
}

// Parsing

impl FromStr for Direction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "U" => Ok(Direction::Up),
            "R" => Ok(Direction::Right),
            "D" => Ok(Direction::Down),
            "L" => Ok(Direction::Left),
            e => Err(format!("Unknown direction char: {}", e)),
        }
    }
}

impl FromStr for Section {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let direction = s[..1].parse()?;
        let distance = s[1..].parse::<u32>().map_err(|e| e.to_string())?;

        Ok(Self {
            direction,
            distance,
        })
    }
}

impl FromStr for Wire {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut sections = Vec::new();

        for section in s.split(",") {
            sections.push(section.parse()?);
        }

        Ok(Self { sections })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::error::Error;

    #[test]
    fn example_task1() -> Result<(), String> {
        let wire1: Wire = "R75,D30,R83,U83,L12,D49,R71,U7,L72".parse()?;
        let wire2: Wire = "U62,R66,U55,R34,D71,R55,D58,R83".parse()?;

        let point_sections = (wire1.to_point_sections(), wire2.to_point_sections());
        //dbg!(&point_sections);
        let overlapping = find_overlapping(&point_sections);
        dbg!(&overlapping);
        let shortest_distance = overlapping
            .iter()
            .map(|(point, _)| point.manhattan_distance_from_default())
            .fold(
                i32::max_value(),
                |lowest, next| {
                    if lowest < next {
                        lowest
                    } else {
                        next
                    }
                },
            );

        assert_eq!(shortest_distance, 159);

        Ok(())
    }

    #[test]
    fn overlaps() {
        let section1 = PointSection {
            section: Section {
                direction: Direction::Up,
                distance: 3,
            },
            start: Point { x: 2, y: 0 },
            end: Point { x: 2, y: 2 },
            steps_to_start: 0,
        };
        let section2 = PointSection {
            section: Section {
                direction: Direction::Right,
                distance: 3,
            },
            start: Point { x: 1, y: 1 },
            end: Point { x: 3, y: 1 },
            steps_to_start: 0,
        };

        assert_eq!(section1.overlaps(&section2), Some(Point { x: 2, y: 1 }));
        assert_eq!(section2.overlaps(&section1), Some(Point { x: 2, y: 1 }));

        let section1 = PointSection {
            section: Section {
                direction: Direction::Down,
                distance: 3,
            },
            start: Point { x: 2, y: 2 },
            end: Point { x: 2, y: 0 },
            steps_to_start: 0,
        };
        let section2 = PointSection {
            section: Section {
                direction: Direction::Left,
                distance: 3,
            },
            start: Point { x: 3, y: 1 },
            end: Point { x: 1, y: 1 },
            steps_to_start: 0,
        };

        assert_eq!(section1.overlaps(&section2), Some(Point { x: 2, y: 1 }));
        assert_eq!(section2.overlaps(&section1), Some(Point { x: 2, y: 1 }));
    }

    #[test]
    fn overlaps_2() {
        let section1 = PointSection {
            section: Section {
                direction: Direction::Up,
                distance: 83,
            },
            start: Point { x: 158, y: -30 },
            end: Point { x: 158, y: 53 },
            steps_to_start: 0,
        };
        let section2 = PointSection {
            section: Section {
                direction: Direction::Right,
                distance: 71,
            },
            start: Point { x: 146, y: 4 },
            end: Point { x: 217, y: 4 },
            steps_to_start: 0,
        };

        assert_eq!(section1.overlaps(&section2), Some(Point { x: 158, y: 4 }));
        assert_eq!(section2.overlaps(&section1), Some(Point { x: 158, y: 4 }));
    }

    #[test]
    fn overlaps_3() {
        let section1 = PointSection {
            section: Section {
                direction: Direction::Right,
                distance: 55,
            },
            start: Point { x: 100, y: 46 },
            end: Point { x: 155, y: 46 },
            steps_to_start: 0,
        };
        let section2 = PointSection {
            section: Section {
                direction: Direction::Down,
                distance: 49,
            },
            start: Point { x: 146, y: 53 },
            end: Point { x: 146, y: 4 },
            steps_to_start: 0,
        };

        assert_eq!(section1.overlaps(&section2), Some(Point { x: 146, y: 46 }));
        assert_eq!(section2.overlaps(&section1), Some(Point { x: 146, y: 46 }));
    }

    #[test]
    fn overlaps_4() {
        let section1 = PointSection {
            section: Section {
                direction: Direction::Left,
                distance: 55,
            },
            start: Point { x: 155, y: 46 },
            end: Point { x: 100, y: 46 },
            steps_to_start: 0,
        };
        let section2 = PointSection {
            section: Section {
                direction: Direction::Up,
                distance: 49,
            },
            start: Point { x: 146, y: 4 },
            end: Point { x: 146, y: 53 },
            steps_to_start: 0,
        };

        assert_eq!(section1.overlaps(&section2), Some(Point { x: 146, y: 46 }));
        assert_eq!(section2.overlaps(&section1), Some(Point { x: 146, y: 46 }));
    }

    #[test]
    fn example_task2() -> Result<(), String> {
        let wire1: Wire = "R75,D30,R83,U83,L12,D49,R71,U7,L72".parse()?;
        let wire2: Wire = "U62,R66,U55,R34,D71,R55,D58,R83".parse()?;

        let point_sections = (wire1.to_point_sections(), wire2.to_point_sections());
        //dbg!(&point_sections);
        let overlapping = find_overlapping(&point_sections);
        dbg!(&overlapping);
        let shortest_distance = overlapping
            .iter()
            .map(|(_, sum_steps)| *sum_steps)
            .fold(
                u32::max_value(),
                |lowest, next| {
                    if lowest < next {
                        lowest
                    } else {
                        next
                    }
                },
            );

        assert_eq!(shortest_distance, 610);

        Ok(())
    }
}
