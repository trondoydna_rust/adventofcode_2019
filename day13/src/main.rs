use std::num::ParseIntError;

mod intcode {
    use std::io::Write;

    #[derive(Clone, Copy, Debug)]
    enum ParamMode {
        Position,
        Immediate,
        Relative,
    }

    #[derive(Clone, Copy, Debug)]
    struct Input {
        value: isize,
    }

    #[derive(Debug)]
    struct Output {
        location: usize,
    }

    #[derive(Debug)]
    enum Operation {
        Add(Input, Input, Output),
        Multiply(Input, Input, Output),
        Input(Output),
        Output(Input),
        JumpIfTrue(Input, Input),
        JumpIfFalse(Input, Input),
        LessThan(Input, Input, Output),
        Equals(Input, Input, Output),
        AdjustRelativeBase(Input),
        Terminate,
    }

    impl Operation {
        fn new(code: usize, run: &mut Program) -> Self {
            match code {
                1 => Operation::Add(run.input(1), run.input(2), run.output(3)),
                2 => Operation::Multiply(run.input(1), run.input(2), run.output(3)),
                3 => Operation::Input(run.output(1)),
                4 => Operation::Output(run.input(1)),
                5 => Operation::JumpIfTrue(run.input(1), run.input(2)),
                6 => Operation::JumpIfFalse(run.input(1), run.input(2)),
                7 => Operation::LessThan(run.input(1), run.input(2), run.output(3)),
                8 => Operation::Equals(run.input(1), run.input(2), run.output(3)),
                9 => Operation::AdjustRelativeBase(run.input(1)),
                99 => Operation::Terminate,
                n => panic!("Unknown opcode {}", n),
            }
        }

        fn execute(&mut self, run: &mut Program) -> Result<Option<isize>, ()> {
            match self {
                Operation::Add(i1, i2, o) => {
                    run.write_to(o, i1.value + i2.value);

                    Ok(None)
                }
                Operation::Multiply(i1, i2, o) => {
                    run.write_to(o, i1.value * i2.value);

                    Ok(None)
                }
                Operation::Input(o) => {
                    if run.early_return_before_input {
                        run.early_return_before_input = false;
                        Err(())
                    } else {
                        run.early_return_before_input = true;
                        let next_move = /*loop {
                            println!("Select next move [-1, 0, 1]: ");
                            std::io::stdout().flush();
                            let mut next_move = String::new();
                            while let Err(err) = std::io::stdin().read_line(&mut next_move) {
                                next_move.clear();
                                eprintln!("Could not parse input: {:?}", err);
                            }
                            let next_move: isize = match next_move.trim().parse() {
                                Ok(m) if m == -1 || m == 0 || m == 1 => m,
                                Ok(m) => {
                                    eprintln!("Invalid move {}", m);
                                    continue;
                                }
                                Err(err) => {
                                    eprintln!("Could not parse input: {:?}", err);
                                    continue;
                                }
                            };
                            break next_move;
                        }*/0;

                        run.write_to(o, next_move);

                        Ok(None)
                    }
                }
                Operation::Output(i) => Ok(Some(i.value)),
                Operation::JumpIfTrue(_, _) | Operation::JumpIfFalse(_, _) => Ok(None),
                Operation::LessThan(i1, i2, o) => {
                    if i1.value < i2.value {
                        run.write_to(o, 1);
                    } else {
                        run.write_to(o, 0);
                    }
                    Ok(None)
                }
                Operation::Equals(i1, i2, o) => {
                    if i1.value == i2.value {
                        run.write_to(o, 1);
                    } else {
                        run.write_to(o, 0);
                    }
                    Ok(None)
                }
                Operation::AdjustRelativeBase(i) => {
                    run.relative_base += i.value;
                    Ok(None)
                }
                Operation::Terminate => Err(()),
            }
        }

        fn next_instruction_index(&self, program: &Program) -> usize {
            let current = program.instruction_index;
            match self {
                Operation::Terminate => 0,
                Operation::Input(_) if program.early_return_before_input => 0,
                Operation::Input(_) | Operation::Output(_) | Operation::AdjustRelativeBase(_) => {
                    current + 2
                }
                Operation::Add(_, _, _)
                | Operation::Multiply(_, _, _)
                | Operation::LessThan(_, _, _)
                | Operation::Equals(_, _, _) => current + 4,
                Operation::JumpIfTrue(i1, i2) => {
                    if i1.value != 0 {
                        i2.value as usize
                    } else {
                        current + 3
                    }
                }
                Operation::JumpIfFalse(i1, i2) => {
                    if i1.value == 0 {
                        i2.value as usize
                    } else {
                        current + 3
                    }
                }
            }
        }
    }

    #[derive(Clone)]
    pub struct Program {
        intcodes: Vec<isize>,
        input: Option<isize>,
        instruction_index: usize,
        relative_base: isize,
        early_return_before_input: bool,
    }

    impl Program {
        pub fn new(intcodes: Vec<isize>) -> Self {
            Self {
                intcodes,
                input: None,
                instruction_index: 0,
                relative_base: 0,
                early_return_before_input: true,
            }
        }

        pub fn run_to_completion(&mut self, input: isize) -> Vec<isize> {
            self.set_input(input);
            self.flatten().collect()
        }

        pub fn set_input(&mut self, input: isize) {
            self.input = Some(input);
        }

        fn input(&mut self, offset: usize) -> Input {
            let mode = self.param_mode(offset);
            let i = self.instruction_index + offset;
            let val = self.read_from(i);

            let value = match mode {
                ParamMode::Position => self.read_from(val as usize),
                ParamMode::Immediate => val,
                ParamMode::Relative => self.read_from((val + self.relative_base) as usize),
            };

            Input { value }
        }

        fn read_from(&mut self, i: usize) -> isize {
            if i > self.intcodes.len() {
                self.intcodes.resize(i + 1, 0);
                0
            } else {
                self.intcodes[i]
            }
        }

        fn output(&self, offset: usize) -> Output {
            let mode = self.param_mode(offset);

            let location = self.intcodes[self.instruction_index + offset] as usize;
            let location = match mode {
                ParamMode::Position => location,
                ParamMode::Relative => (location as isize + self.relative_base) as usize,
                ParamMode::Immediate => panic!("Cannot write with mode immediate"),
            };

            Output { location }
        }

        fn write_to(&mut self, output: &Output, value: isize) {
            let i = output.location;
            if i >= self.intcodes.len() {
                self.intcodes.resize(i + 1, 0);
            }
            self.intcodes[i] = value;
        }

        fn param_mode(&self, offset: usize) -> ParamMode {
            let mut opcode = self.intcodes[self.instruction_index] as usize;

            let mut mode = ParamMode::Position;

            if opcode > 20_000 {
                mode = ParamMode::Relative;
                opcode -= 20_000;
            }
            if opcode > 10_000 {
                mode = ParamMode::Immediate;
                opcode -= 10_000;
            }
            if offset == 3 {
                return mode;
            }

            let mut mode = ParamMode::Position;
            if opcode > 2_000 {
                mode = ParamMode::Relative;
                opcode -= 2_000;
            }
            if opcode > 1_000 {
                mode = ParamMode::Immediate;
                opcode -= 1_000;
            }
            if offset == 2 {
                return mode;
            }

            if opcode > 200 {
                ParamMode::Relative
            } else if opcode > 100 {
                ParamMode::Immediate
            } else {
                ParamMode::Position
            }
        }

        fn opcode(&self) -> usize {
            self.intcodes[self.instruction_index] as usize % 100
        }
    }

    impl<'a> Iterator for Program {
        type Item = Option<isize>;

        fn next(&mut self) -> Option<Self::Item> {
            let opcode = self.opcode();

            let mut operation = Operation::new(opcode, self);

            let next_instruction_index = operation.next_instruction_index(&self);
            let res = operation.execute(self);

            self.instruction_index = next_instruction_index;

            match res {
                Ok(o) => Some(o),
                Err(()) => None,
            }
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_in_out() {
            assert_eq!(
                Program::new(vec![3, 0, 4, 0, 99]).run_to_completion(1),
                vec![1]
            );
        }

        #[test]
        fn test_immediate() {
            assert_eq!(
                Program::new(vec![1002, 4, 3, 4, 33]).run_to_completion(1),
                vec![]
            );
        }

        #[test]
        fn test_immediate_2() {
            assert_eq!(
                Program::new(vec![1101, 100, -1, 4, 0]).run_to_completion(1),
                vec![]
            );
        }

        #[test]
        fn is_equal_to() {
            assert_eq!(
                Program::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(8),
                vec![1]
            );
            assert_eq!(
                Program::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(88),
                vec![0]
            );

            assert_eq!(
                Program::new(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99]).run_to_completion(8),
                vec![1]
            );
            assert_eq!(
                Program::new(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99]).run_to_completion(88),
                vec![0]
            );
        }

        #[test]
        fn is_less_than() {
            assert_eq!(
                Program::new(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(0),
                vec![1]
            );
            assert_eq!(
                Program::new(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(8),
                vec![0]
            );

            assert_eq!(
                Program::new(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99]).run_to_completion(0),
                vec![1]
            );
            assert_eq!(
                Program::new(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99]).run_to_completion(8),
                vec![0]
            );
        }

        #[test]
        fn looping() {
            assert_eq!(
                Program::new(vec![
                    3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9
                ])
                .run_to_completion(0),
                vec![0]
            );
            assert_eq!(
                Program::new(vec![
                    3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9
                ])
                .run_to_completion(111),
                vec![1]
            );

            assert_eq!(
                Program::new(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1])
                    .run_to_completion(0),
                vec![0]
            );
            assert_eq!(
                Program::new(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1])
                    .run_to_completion(111),
                vec![1]
            );
        }

        #[test]
        fn example_day5() {
            let mut program = Program::new(vec![
                3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36,
                98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000,
                1, 20, 4, 20, 1105, 1, 46, 98, 99,
            ]);
            assert_eq!(program.clone().run_to_completion(1), vec![999]);

            assert_eq!(program.clone().run_to_completion(8), vec![1000]);

            assert_eq!(program.run_to_completion(17), vec![1001]);
        }

        #[test]
        fn copy_self() {
            assert_eq!(
                Program::new(vec![
                    109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99
                ])
                .run_to_completion(111),
                vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
            );
        }

        #[test]
        fn output_middle_number() {
            assert_eq!(
                Program::new(vec![104, 1125899906842624, 99]).run_to_completion(111),
                vec![1125899906842624]
            );
        }
    }
}

mod arcade {
    use super::intcode::*;
    use std::fmt::{Display, Error, Formatter};

    #[derive(Debug, Clone, Copy, Eq, PartialEq)]
    pub enum Tile {
        Empty,
        Wall,
        Block,
        HorizontalPaddle,
        Ball,
    }

    impl Tile {
        fn from_code(code: usize) -> Self {
            match code {
                0 => Tile::Empty,
                1 => Tile::Wall,
                2 => Tile::Block,
                3 => Tile::HorizontalPaddle,
                4 => Tile::Ball,
                n => panic!("Unknown tile id {}", n),
            }
        }
    }

    impl Display for Tile {
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
            write!(
                f,
                "{}",
                match self {
                    Tile::Empty => ' ',
                    Tile::Wall => '□',
                    Tile::Block => '▨',
                    Tile::HorizontalPaddle => '▭',
                    Tile::Ball => '◉',
                }
            )
        }
    }

    #[derive(Clone)]
    pub struct ArcadeCabinet {
        pub program: Program,
        pub frame: Vec<Vec<Tile>>,
    }

    impl ArcadeCabinet {
        pub fn new(program: Program) -> Self {
            ArcadeCabinet {
                program,
                frame: vec![],
            }
        }

        pub fn next_frame(&mut self) {
            let program = &mut self.program;
            let frame = program.flatten().try_fold(
                (vec![], 0),
                |(mut frames, index): (
                    Vec<(Option<isize>, Option<isize>, Option<isize>)>,
                    usize,
                ),
                 next| {
                    //dbg!(index);

                    if index >= frames.len() {
                        (index..=frames.len()).for_each(|_| frames.push((None, None, None)));
                    }

                    let (mut x, mut y, mut tile) = frames[index];

                    // check if score
                    if x == Some(-1) && y == Some(0) {
                        println!("Score: {}", next);

                        frames[index] = (None, None, None);

                        Err((frames, index))
                    } else {
                        let mut next_index = index;

                        if x.is_none() {
                            x = Some(next);
                        } else if y.is_none() {
                            y = Some(next);
                        } else {
                            tile = Some(next);
                            next_index += 1;
                        }

                        frames[index] = (x, y, tile);
                        Ok((frames, next_index))
                    }
                },
            );
            let frame = frame.unwrap_or_else(|e| e).0;

            let max_y = frame
                .iter()
                .map(|(_, y, _)| y.unwrap_or(0))
                .max()
                .unwrap_or(0) as usize;

            self.frame = frame.iter().fold(self.frame.clone(), |mut frames, &next| {
                if let (Some(x), Some(y), Some(tile)) = next {
                    let x = x as usize;
                    let y = y as usize;
                    if x >= frames.len() {
                        (x..=frames.len()).for_each(|_| {
                            let mut column = Vec::with_capacity(max_y + 1);
                            column.resize(max_y + 1, Tile::Empty);

                            frames.push(column)
                        });
                    }
                    frames[x][y] = Tile::from_code(tile as usize);
                }

                frames
            });
        }
    }

    impl Display for ArcadeCabinet {
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
            let max_y = self.frame.iter().map(|v| v.len()).max().unwrap_or(0);

            for y in 0..max_y {
                for x in 0..self.frame.len() {
                    write!(f, "{}", self.frame[x][y])?;
                }
                write!(f, "\n")?;
            }

            Ok(())
        }
    }

    impl Iterator for ArcadeCabinet {
        type Item = ArcadeCabinet;

        fn next(&mut self) -> Option<Self::Item> {
            self.next_frame();

            Some(self.clone())
        }
    }
}

fn main() {
    task1();
    println!();
    task2();
}

/// 270
fn task1() {
    use arcade::*;
    use intcode::*;

    let program: Result<Vec<isize>, ParseIntError> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|line| line.parse::<isize>())
        .collect();

    let program = Program::new(program.unwrap());
    let mut arcade = ArcadeCabinet::new(program);
    arcade.next_frame();

    println!("{}", &arcade);
    println!(
        "Task 1 answer is {}",
        arcade
            .frame
            .iter()
            .flatten()
            .filter(|&&t| t == Tile::Block)
            .count()
    );
}

/// 12535
fn task2() {
    use arcade::*;
    use intcode::*;

    let program: Result<Vec<isize>, ParseIntError> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|line| line.parse::<isize>())
        .collect();
    let mut program = program.unwrap();
    program[0] = 2;

    let mut program = Program::new(program);
    program.set_input(0);

    let mut arcade = ArcadeCabinet::new(program);

    while let Some(state) = arcade.next() {
        println!("{}", state);
    }
}
