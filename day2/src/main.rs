#[derive(Debug)]
enum Operation {
    Add,
    Multiply,
    Terminate,
}

impl Operation {
    fn from_code(code: usize) -> Self {
        match code {
            1 => Operation::Add,
            2 => Operation::Multiply,
            99 => Operation::Terminate,
            n => panic!("Unknown opcode {}", n),
        }
    }

    fn calc(&self, input1: usize, input2: usize) -> Option<usize> {
        match &self {
            Operation::Add => Some(input1 + input2),
            Operation::Multiply => Some(input1 * input2),
            Operation::Terminate => None,
        }
    }
}

fn main() {
    task1();
    task2();
}

/// 3654868
fn task1() {
    let initial: Vec<usize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<usize>().unwrap())
        .collect();

    let res = run(&initial, 12, 2);

    println!("Task 1 answer: {}", res);
}

fn run(initial: &Vec<usize>, noun: usize, verb: usize) -> usize {
    let mut ints = initial.clone();
    ints[1] = noun;
    ints[2] = verb;

    let mut i = 0;

    loop {
        let op = Operation::from_code(ints[i]);
        let input1_index = ints[i + 1];
        let input2_index = ints[i + 2];
        let output_index = ints[i + 3];
        let input1 = ints[input1_index];
        let input2 = ints[input2_index];
        let output = op.calc(input1, input2);

        if let Some(res) = output {
            //dbg!(i, input1_index, input2_index, output_index, op, input1, input2, res);
            ints[output_index] = res;
            i += 4;
        } else {
            break;
        }
    }

    ints[0]
}

///
fn task2() {
    let initial: Vec<usize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<usize>().unwrap())
        .collect();

    for noun in 0..=99 {
        for verb in 0..=99 {
            let res = run(&initial, noun, verb);

            if res == 19690720 {
                println!("Task 1 answer: {}", 100 * noun + verb);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_() {}
}
