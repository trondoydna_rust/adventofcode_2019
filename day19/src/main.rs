mod intcode {
    #[derive(Clone, Copy, Debug)]
    enum ParamMode {
        Position,
        Immediate,
        Relative,
    }

    #[derive(Clone, Copy, Debug)]
    struct Read {
        value: isize,
    }

    #[derive(Debug)]
    struct Write {
        location: usize,
    }

    #[derive(Debug)]
    enum Operation {
        Add(Read, Read, Write),
        Multiply(Read, Read, Write),
        Input(Write),
        Output(Read),
        JumpIfTrue(Read, Read),
        JumpIfFalse(Read, Read),
        LessThan(Read, Read, Write),
        Equals(Read, Read, Write),
        AdjustRelativeBase(Read),
        Terminate,
    }

    impl Operation {
        fn new(code: usize, run: &mut Program) -> Self {
            match code {
                1 => Operation::Add(run.new_read(1), run.new_read(2), run.new_write(3)),
                2 => Operation::Multiply(run.new_read(1), run.new_read(2), run.new_write(3)),
                3 => Operation::Input(run.new_write(1)),
                4 => Operation::Output(run.new_read(1)),
                5 => Operation::JumpIfTrue(run.new_read(1), run.new_read(2)),
                6 => Operation::JumpIfFalse(run.new_read(1), run.new_read(2)),
                7 => Operation::LessThan(run.new_read(1), run.new_read(2), run.new_write(3)),
                8 => Operation::Equals(run.new_read(1), run.new_read(2), run.new_write(3)),
                9 => Operation::AdjustRelativeBase(run.new_read(1)),
                99 => Operation::Terminate,
                n => panic!("Unknown opcode {}", n),
            }
        }

        fn execute(&mut self, run: &mut Program) -> Result<Option<isize>, ()> {
            match self {
                Operation::Add(i1, i2, o) => {
                    run.write_to(o, i1.value + i2.value);

                    Ok(None)
                }
                Operation::Multiply(i1, i2, o) => {
                    run.write_to(o, i1.value * i2.value);

                    Ok(None)
                }
                Operation::Input(o) => {
                    run.write_to(o, run.input[run.input_index]);

                    run.input_index += 1;

                    Ok(None)
                }
                Operation::Output(i) => Ok(Some(i.value)),
                Operation::JumpIfTrue(_, _) | Operation::JumpIfFalse(_, _) => Ok(None),
                Operation::LessThan(i1, i2, o) => {
                    if i1.value < i2.value {
                        run.write_to(o, 1);
                    } else {
                        run.write_to(o, 0);
                    }
                    Ok(None)
                }
                Operation::Equals(i1, i2, o) => {
                    if i1.value == i2.value {
                        run.write_to(o, 1);
                    } else {
                        run.write_to(o, 0);
                    }
                    Ok(None)
                }
                Operation::AdjustRelativeBase(i) => {
                    run.relative_base += i.value;
                    Ok(None)
                }
                Operation::Terminate => Err(()),
            }
        }

        fn next_instruction_index(&self, current: usize) -> usize {
            match self {
                Operation::Terminate => 0,
                Operation::Input(_) | Operation::Output(_) | Operation::AdjustRelativeBase(_) => {
                    current + 2
                }
                Operation::Add(_, _, _)
                | Operation::Multiply(_, _, _)
                | Operation::LessThan(_, _, _)
                | Operation::Equals(_, _, _) => current + 4,
                Operation::JumpIfTrue(i1, i2) => {
                    if i1.value != 0 {
                        i2.value as usize
                    } else {
                        current + 3
                    }
                }
                Operation::JumpIfFalse(i1, i2) => {
                    if i1.value == 0 {
                        i2.value as usize
                    } else {
                        current + 3
                    }
                }
            }
        }
    }

    #[derive(Clone)]
    pub struct Program {
        intcodes: Vec<isize>,
        input: Vec<isize>,
        input_index: usize,
        instruction_index: usize,
        relative_base: isize,
    }

    impl Program {
        pub fn new(intcodes: Vec<isize>) -> Self {
            Self {
                intcodes,
                input: vec![],
                input_index: 0,
                instruction_index: 0,
                relative_base: 0,
            }
        }

        pub fn run_to_completion(&mut self, input: isize) -> Vec<isize> {
            self.set_input(vec![input]);
            self.collect()
        }

        pub fn set_input(&mut self, input: Vec<isize>) {
            self.input = input;
            self.input_index = 0;
        }

        fn new_read(&mut self, offset: usize) -> Read {
            let mode = self.param_mode(offset);
            let i = self.instruction_index + offset;
            let val = self.read_from(i);

            let value = match mode {
                ParamMode::Position => self.read_from(val as usize),
                ParamMode::Immediate => val,
                ParamMode::Relative => self.read_from((val + self.relative_base) as usize),
            };

            Read { value }
        }

        fn read_from(&mut self, i: usize) -> isize {
            if i > self.intcodes.len() {
                self.intcodes.resize(i + 1, 0);
                0
            } else {
                self.intcodes[i]
            }
        }

        fn new_write(&self, offset: usize) -> Write {
            let mode = self.param_mode(offset);

            let location = self.intcodes[self.instruction_index + offset] as usize;
            let location = match mode {
                ParamMode::Position => location,
                ParamMode::Relative => (location as isize + self.relative_base) as usize,
                ParamMode::Immediate => panic!("Cannot write with mode immediate"),
            };

            Write { location }
        }

        fn write_to(&mut self, output: &Write, value: isize) {
            let i = output.location;
            if i >= self.intcodes.len() {
                self.intcodes.resize(i + 1, 0);
            }
            self.intcodes[i] = value;
        }

        fn param_mode(&self, offset: usize) -> ParamMode {
            let mut opcode = self.intcodes[self.instruction_index] as usize;

            let mut mode = ParamMode::Position;

            if opcode > 20_000 {
                mode = ParamMode::Relative;
                opcode -= 20_000;
            }
            if opcode > 10_000 {
                mode = ParamMode::Immediate;
                opcode -= 10_000;
            }
            if offset == 3 {
                return mode;
            }

            let mut mode = ParamMode::Position;
            if opcode > 2_000 {
                mode = ParamMode::Relative;
                opcode -= 2_000;
            }
            if opcode > 1_000 {
                mode = ParamMode::Immediate;
                opcode -= 1_000;
            }
            if offset == 2 {
                return mode;
            }

            if opcode > 200 {
                ParamMode::Relative
            } else if opcode > 100 {
                ParamMode::Immediate
            } else {
                ParamMode::Position
            }
        }

        fn opcode(&self) -> usize {
            self.intcodes[self.instruction_index] as usize % 100
        }
    }

    impl<'a> Iterator for Program {
        type Item = isize;

        fn next(&mut self) -> Option<Self::Item> {
            loop {
                let opcode = self.opcode();

                let mut operation = Operation::new(opcode, self);

                let next_instruction_index =
                    operation.next_instruction_index(self.instruction_index);
                let res = operation.execute(self);

                self.instruction_index = next_instruction_index;

                match res {
                    Ok(next) => match next {
                        Some(o) => break Some(o),
                        None => continue,
                    },
                    Err(()) => break None,
                }
            }
        }
    }
    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_in_out() {
            assert_eq!(
                Program::new(vec![3, 0, 4, 0, 99]).run_to_completion(1),
                vec![1]
            );
        }

        #[test]
        fn test_immediate() {
            assert_eq!(
                Program::new(vec![1002, 4, 3, 4, 33]).run_to_completion(1),
                vec![]
            );
        }

        #[test]
        fn test_immediate_2() {
            assert_eq!(
                Program::new(vec![1101, 100, -1, 4, 0]).run_to_completion(1),
                vec![]
            );
        }

        #[test]
        fn is_equal_to() {
            assert_eq!(
                Program::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(8),
                vec![1]
            );
            assert_eq!(
                Program::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(88),
                vec![0]
            );

            assert_eq!(
                Program::new(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99]).run_to_completion(8),
                vec![1]
            );
            assert_eq!(
                Program::new(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99]).run_to_completion(88),
                vec![0]
            );
        }

        #[test]
        fn is_less_than() {
            assert_eq!(
                Program::new(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(0),
                vec![1]
            );
            assert_eq!(
                Program::new(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(8),
                vec![0]
            );

            assert_eq!(
                Program::new(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99]).run_to_completion(0),
                vec![1]
            );
            assert_eq!(
                Program::new(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99]).run_to_completion(8),
                vec![0]
            );
        }

        #[test]
        fn looping() {
            assert_eq!(
                Program::new(vec![
                    3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9
                ])
                .run_to_completion(0),
                vec![0]
            );
            assert_eq!(
                Program::new(vec![
                    3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9
                ])
                .run_to_completion(111),
                vec![1]
            );

            assert_eq!(
                Program::new(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1])
                    .run_to_completion(0),
                vec![0]
            );
            assert_eq!(
                Program::new(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1])
                    .run_to_completion(111),
                vec![1]
            );
        }

        #[test]
        fn example_day5() {
            let mut program = Program::new(vec![
                3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36,
                98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000,
                1, 20, 4, 20, 1105, 1, 46, 98, 99,
            ]);
            assert_eq!(program.clone().run_to_completion(1), vec![999]);

            assert_eq!(program.clone().run_to_completion(8), vec![1000]);

            assert_eq!(program.run_to_completion(17), vec![1001]);
        }

        #[test]
        fn copy_self() {
            assert_eq!(
                Program::new(vec![
                    109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99
                ])
                .run_to_completion(111),
                vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
            );
        }

        #[test]
        fn output_middle_number() {
            assert_eq!(
                Program::new(vec![104, 1125899906842624, 99]).run_to_completion(111),
                vec![1125899906842624]
            );
        }
    }
}

fn main() {
    task1();
    task2();
}

/// 199
fn task1() {
    let count = run(50).iter().flatten().filter(|&&o| o).count();

    println!("Task 1 answer is {:?}", count);
}

/// 10180726
fn task2() {
    let matrix = run(1_100);

    let count = matrix
        .iter()
        .enumerate()
        .filter(|&(x, _)| x + 99 < matrix.len() - 1)
        .map(|(x, c)| {
            if let Some(y) = c
                .iter()
                .enumerate()
                .filter(|(_, &o)| o)
                .map(|(y, _)| y)
                .filter(|&y| y + 99 < matrix.len() - 1)
                .find(|&y| {
                    matrix[x][y]
                        && matrix[x + 99][y]
                        && matrix[x][y + 99]
                        && matrix[x + 99][y + 99]
                })
            {
                Some((x, y))
            } else {
                None
            }
        })
        .find(|o| o.is_some())
        .unwrap()
        .unwrap();

    println!("Task 2 answer is {:?}", count);
}

fn run(tile_width: isize) -> Vec<Vec<bool>> {
    let intcodes: Vec<isize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<isize>().unwrap())
        .collect();

    let mut tile_counter: isize = 0;
    let mut matrix = Vec::new();
    matrix.resize(tile_width as usize, Vec::new());

    loop {
        let x = tile_counter % tile_width;
        let y = tile_counter / tile_width;

        let mut program = intcode::Program::new(intcodes.clone());
        program.set_input(vec![x, y]);

        let out = program.next().unwrap();

        matrix[x as usize].push(out == 1);
        tile_counter += 1;

        if tile_counter >= tile_width * tile_width {
            break matrix;
        }
        if tile_counter % (100_000) == 0 {
            let progress_percent = tile_counter as f32 / (tile_width * tile_width) as f32;
            dbg!(progress_percent);
        }
    }
}
