use std::collections::HashMap;
use std::str::FromStr;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Object {
    id: String,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Orbit {
    center: Object,
    orbiter: Object,
}

fn main() {
    task1();
    task2();
}

/// 312697
fn task1() {
    let orbit: HashMap<Object, Object> = std::fs::read_to_string("input")
        .unwrap()
        .split_whitespace()
        .map(|line| line.parse::<Orbit>().unwrap())
        .map(|orbit| (orbit.orbiter, orbit.center))
        .collect();

    let sum = sum(orbit);

    println!("Task 1 answer: {}", sum);
}

fn sum(orbit: HashMap<Object, Object>) -> u32 {
    let to = Object {
        id: "COM".to_string(),
    };

    orbit
        .values()
        .map(|center| distance_to(&orbit, center, &to))
        .sum()
}

fn distance_to(orbit: &HashMap<Object, Object>, from: &Object, to: &Object) -> u32 {
    if from == to {
        return 1;
    }

    let mut center = from;
    let mut steps = 1;

    loop {
        // Get the center of current center
        if let Some(c) = orbit.get(center) {
            steps += 1;

            if c == to {
                break steps;
            }

            center = c;
        } else {
            panic!(
                "Did to find node {:?} after reaching the end in {} steps",
                to, steps
            );
        }
    }
}

///
fn task2() {
    let orbits: HashMap<Object, Object> = std::fs::read_to_string("input")
        .unwrap()
        .split_whitespace()
        .map(|line| line.parse::<Orbit>().unwrap())
        .map(|orbit| (orbit.orbiter, orbit.center))
        .collect();
    let transfers = transfers(&orbits);

    println!("Task 2 answer: {}", transfers);
}

fn transfers(orbits: &HashMap<Object, Object>) -> usize {
    let you = Object {
        id: "YOU".to_string(),
    };
    let san = Object {
        id: "SAN".to_string(),
    };
    let com = Object {
        id: "COM".to_string(),
    };

    let nodes_you_to_com = objects_to(&orbits, &you, &com);
    let nodes_san_to_com = objects_to(&orbits, &san, &com);
    let first_common_node = find_first_common_object(&nodes_you_to_com, &nodes_san_to_com);
    let nodes_you_to_first = objects_to(&orbits, &you, &first_common_node);
    let nodes_san_to_first = objects_to(&orbits, &san, &first_common_node);
    let transfers = nodes_you_to_first.len() + nodes_san_to_first.len();

    transfers
}

fn objects_to(orbits: &HashMap<Object, Object>, from: &Object, to: &Object) -> Vec<Object> {
    let mut center = from;
    let mut objects = Vec::new();

    loop {
        // Get the center of current center
        if let Some(c) = orbits.get(center) {
            if c == to {
                break objects;
            }

            objects.push(c.clone());
            center = c;
        } else {
            panic!(
                "Did to find node {:?} after reaching the end recoding these objects: {:?}",
                to, &objects
            );
        }
    }
}

fn find_first_common_object<'a>(you: &'a Vec<Object>, san: &'a Vec<Object>) -> &'a Object {
    you.iter()
        .map(|y_object| (y_object, san.iter().position(|s_object| y_object == s_object)))
        .filter(|(_, i)| i.is_some())
        .map(|(o, i)| (o, i.unwrap()))
        .min_by(|(_, i1), (_, i2)| i1.cmp(i2))
        .unwrap()
        .0
}

// parsing

impl FromStr for Orbit {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = s.split(")").collect();
        let center = split[0].parse()?;
        let orbiter = split[1].parse()?;

        Ok(Self { center, orbiter })
    }
}

impl FromStr for Object {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self { id: s.to_string() })
    }
}

mod tests {
    use super::*;

    #[test]
    fn example_task_1() {
        let input = "COM)B B)C C)D D)E E)F B)G G)H D)I E)J J)K K)L"
            .split_whitespace()
            .map(|line| line.parse::<Orbit>().unwrap())
            .map(|orbit| (orbit.orbiter, orbit.center))
            .collect();

        assert_eq!(sum(input), 42);
    }

    #[test]
    fn example_task_2() {
        let input = "COM)B B)C C)D D)E E)F B)G G)H D)I E)J J)K K)L K)YOU I)SAN"
            .split_whitespace()
            .map(|line| line.parse::<Orbit>().unwrap())
            .map(|orbit| (orbit.orbiter, orbit.center))
            .collect();

        assert_eq!(transfers(&input), 4);
    }
}
