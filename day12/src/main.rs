mod one {
    use std::cmp::Ordering;

    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub struct Position {
        pub x: i32,
        pub y: i32,
        pub z: i32,
    }

    impl Position {
        pub fn apply_velocity(&self, velocity: &Velocity) -> Self {
            Self {
                x: self.x + velocity.x,
                y: self.y + velocity.y,
                z: self.z + velocity.z,
            }
        }

        pub fn calc_potential_energy(&self) -> u32 {
            (self.x.abs() + self.y.abs() + self.z.abs()) as u32
        }
    }

    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub struct Velocity {
        x: i32,
        y: i32,
        z: i32,
    }

    impl Velocity {
        pub fn new() -> Self {
            Velocity { x: 0, y: 0, z: 0 }
        }

        pub fn apply_gravity(&self, self_position: &Position, other_position: &Position) -> Self {
            Self {
                x: Self::inc(self.x, self_position.x.cmp(&other_position.x)),
                y: Self::inc(self.y, self_position.y.cmp(&other_position.y)),
                z: Self::inc(self.z, self_position.z.cmp(&other_position.z)),
            }
        }

        fn inc(val: i32, cmp: Ordering) -> i32 {
            match cmp {
                Ordering::Less => val + 1,
                Ordering::Equal => val,
                Ordering::Greater => val - 1,
            }
        }

        pub fn calc_kinetic_energy(&self) -> u32 {
            (self.x.abs() + self.y.abs() + self.z.abs()) as u32
        }
    }

    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub struct Moon {
        position: Position,
        velocity: Velocity,
    }

    impl Moon {
        pub fn start(position: Position) -> Self {
            Self {
                position,
                velocity: Velocity::new(),
            }
        }

        pub fn calc_total_energy(&self) -> u32 {
            self.position.calc_potential_energy() * self.velocity.calc_kinetic_energy()
        }
    }

    pub fn move_moons(moons: &Vec<Moon>, steps: u32) -> Vec<Moon> {
        (0..steps).fold(moons.clone(), |moons: Vec<Moon>, _| move_moons_once(moons))
    }

    pub fn move_moons_once(moons: Vec<Moon>) -> Vec<Moon> {
        (0..moons.len())
            .map(|i| {
                let current_moon = moons[i];

                let velocity = (0..moons.len())
                    .filter(|&j| i != j)
                    .map(|j| moons[j])
                    .fold(
                        (current_moon.velocity, current_moon.position),
                        |(vel, pos), other_moon| {
                            (vel.apply_gravity(&pos, &other_moon.position), pos)
                        },
                    )
                    .0;

                let position = current_moon.position.apply_velocity(&velocity);

                Moon { position, velocity }
            })
            .collect()
    }

    pub fn calc_total_energy(moons: &Vec<Moon>) -> u32 {
        moons.iter().map(|m| m.calc_total_energy()).sum()
    }

    mod tests {
        use super::*;

        #[test]
        fn example() {
            let moons = vec![
                Moon::start(Position { x: -1, y: 0, z: 2 }),
                Moon::start(Position {
                    x: 2,
                    y: -10,
                    z: -7,
                }),
                Moon::start(Position { x: 4, y: -8, z: 8 }),
                Moon::start(Position { x: 3, y: 5, z: -1 }),
            ];

            let moons = move_moons(&moons, 10);

            dbg!(&moons);

            assert_eq!(calc_total_energy(&moons), 179);
        }
    }
}

mod two {
    use std::cmp::Ordering;

    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub struct SinglePosition {
        pub x: i32,
    }

    impl SinglePosition {
        fn apply_velocity(&self, velocity: &SingleVelocity) -> Self {
            Self {
                x: self.x + velocity.x,
            }
        }
    }

    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub struct SingleVelocity {
        x: i32,
    }

    impl SingleVelocity {
        pub fn new() -> Self {
            SingleVelocity { x: 0 }
        }

        pub fn apply_gravity(
            &self,
            self_position: &SinglePosition,
            other_position: &SinglePosition,
        ) -> Self {
            Self {
                x: Self::inc(self.x, self_position.x.cmp(&other_position.x)),
            }
        }

        fn inc(val: i32, cmp: Ordering) -> i32 {
            match cmp {
                Ordering::Less => val + 1,
                Ordering::Equal => val,
                Ordering::Greater => val - 1,
            }
        }

        fn is_zero(&self) -> bool {
            self.x == 0
        }
    }

    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
    pub struct SingleMoon {
        position: SinglePosition,
        velocity: SingleVelocity,
    }

    impl SingleMoon {
        pub fn start(position: SinglePosition) -> Self {
            Self {
                position,
                velocity: SingleVelocity::new(),
            }
        }
    }

    /// Making an assumption that it does not repeat until back to start
    pub fn move_moons_until_repeated(single_moons: Vec<SingleMoon>) -> u128 {
        ((0..)
            .try_fold(single_moons.clone(), |previous, n| {
                let next = move_moons_once(previous);

                if n % 100_000 == 0 {
                    println!("{}", n);
                }

                if next.iter().all(|m| m.velocity.is_zero()) && next == single_moons {
                    Err(n)
                } else {
                    Ok(next)
                }
            })
            .unwrap_err()
            + 1)
    }

    pub fn move_moons_once(moons: Vec<SingleMoon>) -> Vec<SingleMoon> {
        (0..moons.len())
            .map(|i| {
                let current_moon = moons[i];

                let velocity = (0..moons.len())
                    .filter(|&j| i != j)
                    .map(|j| moons[j])
                    .fold(
                        (current_moon.velocity, current_moon.position),
                        |(vel, pos), other_moon| {
                            (vel.apply_gravity(&pos, &other_moon.position), pos)
                        },
                    )
                    .0;

                let position = current_moon.position.apply_velocity(&velocity);

                SingleMoon { position, velocity }
            })
            .collect()
    }

    mod tests {
        use super::*;

        #[test]
        fn example() {
            let moons = vec![
                SingleMoon::start(SinglePosition { x: -1 }),
                SingleMoon::start(SinglePosition { x: 2 }),
                SingleMoon::start(SinglePosition { x: 4 }),
                SingleMoon::start(SinglePosition { x: 3 }),
            ];

            assert_eq!(move_moons_until_repeated(moons), 2_772);
        }

        #[test]
        fn example2() {
            let moons = vec![
                SingleMoon::start(SinglePosition { x: -8 }),
                SingleMoon::start(SinglePosition { x: 5 }),
                SingleMoon::start(SinglePosition { x: 2 }),
                SingleMoon::start(SinglePosition { x: 9 }),
            ];

            assert_eq!(move_moons_until_repeated(moons), 4_686_774_924);
        }
    }
}

fn main() {
    task1();
    task2();
}

/// 11384
fn task1() {
    use one::*;
    /*
    <x= -7, y= -1, z=  6>
    <x=  6, y= -9, z= -9>
    <x=-12, y=  2, z= -7>
    <x=  4, y=-17, z=-12>
    */
    let moons = vec![
        Moon::start(Position { x: -7, y: -1, z: 6 }),
        Moon::start(Position { x: 6, y: -9, z: -9 }),
        Moon::start(Position {
            x: -12,
            y: 2,
            z: -7,
        }),
        Moon::start(Position {
            x: 4,
            y: -17,
            z: -12,
        }),
    ];

    let moons = move_moons(&moons, 1000);
    let total_energy = calc_total_energy(&moons);

    println!("Task 1 answer is {}", total_energy);
}

fn task2() {
    use two::*;

    let moons = vec![
        SingleMoon::start(SinglePosition { x: -7 }),
        SingleMoon::start(SinglePosition { x: 6 }),
        SingleMoon::start(SinglePosition { x: -12 }),
        SingleMoon::start(SinglePosition { x: 4 }),
    ];
    let steps = move_moons_until_repeated(moons);

    println!("Task 2 answer is {}", steps);
}
