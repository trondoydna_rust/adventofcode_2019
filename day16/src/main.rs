use std::iter;
use std::num::ParseIntError;

fn main() {
    let signal: Result<Vec<u8>, ParseIntError> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split("")
        .filter(|c| !c.is_empty())
        .map(|c| c.parse::<u8>())
        .collect();

    task1(signal.clone().unwrap());
    task2(signal.unwrap());
}

/// 18933364
fn task1(signal: Vec<u8>) {
    let result = run(signal, 100);
    let first8: String = result.into_iter().take(8).map(|n| n.to_string()).collect();

    println!("Task 1 answer is {:?}", first8);
}

/// Too slow...
fn task2(signal: Vec<u8>) {
    let result = run_repeated(signal);

    println!("Task 2 answer is {}", result);
}

fn run_repeated(signal: Vec<u8>) -> String {
    let signal = iter::repeat(signal).take(10_000).flatten().collect();
    let result = run(signal, 100);
    let offset = result
        .iter()
        .take(7)
        .map(|&n| n.to_string())
        .collect::<String>()
        .parse::<usize>()
        .unwrap();
    let result: String = result
        .iter()
        .skip(offset)
        .take(8)
        .map(|n| n.to_string())
        .collect();

    result
}

fn run(signal: Vec<u8>, n: usize) -> Vec<u8> {
    (0..n).fold(signal, |previous, _| calc_output(previous))
}

fn calc_output(signal: Vec<u8>) -> Vec<u8> {
    signal
        .iter()
        .enumerate()
        .map(|(i, _)| {
            if i % 1000 == 0 && i != 0 {
                println!("{}", i);
            }

            let sum: i32 = signal
                .iter()
                .zip(generate_pattern(i + 1))
                .map(|(&s, p)| {
                    //dbg!(s, p);

                    s as i32 * p
                })
                .sum();
            let sum = (sum.abs() % 10) as u8;

            sum
        })
        .collect()
}

fn generate_pattern(i: usize) -> impl Iterator<Item = i32> {
    let inner_pattern = iter::repeat(0)
        .take(i)
        .chain(iter::repeat(1).take(i))
        .chain(iter::repeat(0).take(i))
        .chain(iter::repeat(-1).take(i));

    iter::repeat(inner_pattern).flatten().skip(1)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::num::ParseIntError;

    #[test]
    fn short_example_task1() {
        let signal: Result<Vec<u8>, ParseIntError> = "12345678"
            .split("")
            .filter(|c| !c.is_empty())
            .map(|c| c.parse::<u8>())
            .collect();
        let result = run(signal.unwrap(), 1);
        let first8: String = result.into_iter().take(8).map(|n| n.to_string()).collect();

        assert_eq!(first8, "48226158");
    }

    #[test]
    fn examples_task1() {
        let signal: Result<Vec<u8>, ParseIntError> = "80871224585914546619083218645595"
            .split("")
            .filter(|c| !c.is_empty())
            .map(|c| c.parse::<u8>())
            .collect();

        let result = run(signal.unwrap(), 100);
        let first8: String = result.into_iter().take(8).map(|n| n.to_string()).collect();

        assert_eq!(first8, "24176176");
    }

    #[test]
    fn examples_task2() {
        let signal: Result<Vec<u8>, ParseIntError> = "03036732577212944063491565474664"
            .split("")
            .filter(|c| !c.is_empty())
            .map(|c| c.parse::<u8>())
            .collect();

        let result = run_repeated(signal.unwrap());

        assert_eq!(result, "84462026");
    }
}
