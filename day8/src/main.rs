use std::fmt::{Display, Error, Formatter};
use std::iter::FromIterator;

const IMAGE_WIDTH: usize = 25;
const IMAGE_HEIGHT: usize = 6;

#[derive(Clone, Copy, Debug, Ord, PartialOrd, Eq, PartialEq)]
enum Colour {
    Black,
    White,
    Transparent,
}

impl Colour {
    fn from_code(code: u8) -> Self {
        match code {
            0 => Colour::Black,
            1 => Colour::White,
            2 => Colour::Transparent,
            _ => panic!("Unknown colour code {}", code),
        }
    }
}

impl Display for Colour {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(
            f, "{}",
            match self {
                Colour::Black => "■",
                Colour::White => "□",
                Colour::Transparent => "⛝",
            }
        )
    }
}

#[derive(Debug)]
struct Layer {
    grid: [[Colour; IMAGE_HEIGHT]; IMAGE_WIDTH],
}

impl Layer {
    fn black() -> Self {
        Self {
            grid: [[Colour::Black; IMAGE_HEIGHT]; IMAGE_WIDTH],
        }
    }

    fn transparent() -> Self {
        Self {
            grid: [[Colour::Transparent; IMAGE_HEIGHT]; IMAGE_WIDTH],
        }
    }

    fn count_non_zero(&self) -> usize {
        self.grid
            .iter()
            .flatten()
            .filter(|&&d| d != Colour::Black)
            .count()
    }

    fn count(&self, colour: Colour) -> usize {
        self.grid.iter().flatten().filter(|&&d| d == colour).count()
    }
}

impl Display for Layer {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        for y in 0..IMAGE_HEIGHT {
            for x in 0..IMAGE_WIDTH {
                write!(f, "{}", &self.grid[x][y])?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

#[derive(Debug)]
struct Image {
    layers: Vec<Layer>,
}

impl Image {
    fn empty() -> Self {
        Self { layers: Vec::new() }
    }
}

impl FromIterator<Colour> for Image {
    fn from_iter<T: IntoIterator<Item = Colour>>(iter: T) -> Self {
        iter.into_iter()
            .fold((Image::empty(), 0, 0, 0), |state, next| {
                let mut image = state.0;
                let mut l = state.1;
                let mut x = state.2;
                let mut y = state.3;

                if l >= image.layers.len() {
                    image.layers.push(Layer::black());
                }
                image.layers[l].grid[x][y] = next;

                if x < IMAGE_WIDTH - 1 {
                    x += 1;
                } else if y < IMAGE_HEIGHT - 1 {
                    x = 0;
                    y += 1;
                } else {
                    x = 0;
                    y = 0;
                    l += 1;
                }

                (image, l, x, y)
            })
            .0
    }
}

impl IntoIterator for Image {
    type Item = (Colour, usize, usize);
    type IntoIter = ImageIter;

    fn into_iter(self) -> Self::IntoIter {
        ImageIter {
            image: self,
            l: 0,
            x: 0,
            y: 0,
        }
    }
}

struct ImageIter {
    image: Image,
    l: usize,
    x: usize,
    y: usize,
}

impl Iterator for ImageIter {
    type Item = (Colour, usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        let next = self.image.layers[self.l].grid[self.x][self.y];

        if self.x < IMAGE_WIDTH - 1 {
            self.x += 1;
        } else if self.y < IMAGE_HEIGHT - 1 {
            self.x = 0;
            self.y += 1;
        } else if self.l < self.image.layers.len() - 1 {
            self.x = 0;
            self.y = 0;
            self.l += 1;
        } else {
            return None;
        }

        Some((next, self.x, self.y))
    }
}

fn main() {
    task1();
    task2();
}

/// 1965
fn task1() {
    let image = read_image();

    let res = image
        .layers
        .iter()
        .map(|layer| (layer, layer.count_non_zero()))
        .min_by(|(_, count1), (_, count2)| count2.cmp(count1))
        .map(|(layer, _)| layer)
        .map(|layer| layer.count(Colour::White) * layer.count(Colour::Transparent));

    println!("Task 1 answer is {:?}", res);
}

/// GZKJY
fn task2() {
    let layer = read_image()
        .into_iter()
        .fold(Layer::transparent(), |mut layer, next| {
            if layer.grid[next.1][next.2] == Colour::Transparent {
                layer.grid[next.1][next.2] = next.0
            }

            layer
        });

    println!("Task 2 answer is");
    println!("{}", layer);
}

fn read_image() -> Image {
    std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .chars()
        .map(|c| c.to_digit(10).unwrap() as u8)
        .map(|d| Colour::from_code(d))
        .collect::<Image>()
}
