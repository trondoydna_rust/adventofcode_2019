fn main() {
    task1();
    task2();
}

fn task1() {
    let count = (264_793..=803_935)
        .filter(|&n| n >= 100_000 && n <= 999_999)
        .map(|n| {
            n.to_string()
                .chars()
                .map(|c| c.to_digit(10).unwrap())
                .map(|n| n as u8)
                .collect::<Vec<u8>>()
        })
        .filter(|d| {
            let counters = d.iter().fold([0 as u8; 10], |mut counters, &next| {
                counters[next as usize] += 1;

                counters
            });

            counters.iter().any(|&n| n >= 2)
        })
        .filter(|d| {
            let (_, valid) = d.iter().fold((0 as u8, true), |(max, valid), &next| {
                if !valid || next < max {
                    (0, false)
                } else {
                    (next, true)
                }
            });

            valid
        })
        .count();

    println!("Task 1 answer: {}", count);
}

fn task2() {
    let count = (264_793..=803_935)
        .filter(|&n| n >= 100_000 && n <= 999_999)
        .map(|n| {
            n.to_string()
                .chars()
                .map(|c| c.to_digit(10).unwrap())
                .map(|n| n as u8)
                .collect::<Vec<u8>>()
        })
        .filter(|d| {
            let counters = d.iter().fold([0 as u8; 10], |mut counters, &next| {
                counters[next as usize] += 1;

                counters
            });

            counters.iter().any(|&n| n == 2) // The only difference :p (n >= 2 --> n == 2)
        })
        .filter(|d| {
            let (_, valid) = d.iter().fold((0 as u8, true), |(max, valid), &next| {
                if !valid || next < max {
                    (0, false)
                } else {
                    (next, true)
                }
            });

            valid
        })
        .count();

    println!("Task 2 answer: {}", count);
}
