fn main() {
    task1();
    task2();
}

fn fuel_required(mass: u32) -> u32 {
    let t = (mass / 3) as u32;

    if t < 2 {
        0
    } else {
        t - 2
    }
}

/// 3495189
fn task1() {
    let sum: u32 = std::fs::read_to_string("input")
        .unwrap()
        .split_whitespace()
        .map(|line| line.parse::<u32>().unwrap())
        .map(fuel_required)
        .sum();

    println!("Task 1 answer: {}", sum);
}

/// 5239910
fn task2() {
    let mut sum = 0;

    for line in std::fs::read_to_string("input").unwrap().split_whitespace() {
        let mut fuel: u32 = fuel_required(line.parse().unwrap());

        while fuel > 0 {
            sum += fuel;
            fuel = fuel_required(fuel);
        }
    }

    println!("Task 2 answer: {}", sum);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fuel_required() {
        assert_eq!(fuel_required(12), 2);
        assert_eq!(fuel_required(14), 2);
        assert_eq!(fuel_required(1969), 654);
        assert_eq!(fuel_required(100756), 33583);
    }
}
