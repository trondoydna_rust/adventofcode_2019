use std::cell::Cell;
use std::collections::HashMap;
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
struct Chemical {
    code: String,
}

impl Chemical {
    fn new(code: &str) -> Self {
        Self {
            code: code.to_string(),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Quantity {
    chemical: Chemical,
    value: u64,
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Conversion {
    input: Vec<Quantity>,
    output: Quantity,
}

#[derive(Debug)]
struct Conversions {
    conversions: Vec<Conversion>,
}

impl Conversions {
    fn find_conversion_with_output(&self, chemical: &Chemical) -> Option<Conversion> {
        self.conversions
            .iter()
            .find(|c| &c.output.chemical == chemical)
            .cloned()
    }

    fn convert(&self, leftovers: &mut HashMap<Chemical, Cell<u64>>, next: &Conversion) -> Quantity {
        next.input.iter().for_each(|in_q| {
            let o = self.find_conversion_with_output(&in_q.chemical);

            if let Some(out_c) = o {
                let mut amount = leftovers
                    .remove(&in_q.chemical)
                    .unwrap_or(Cell::new(0))
                    .get();
                let amount_needed = in_q.value;

                while amount < amount_needed {
                    amount += self.convert(leftovers, &out_c).value;
                }
                leftovers.insert(in_q.chemical.clone(), Cell::new(amount - amount_needed));
            } else {
                // Only the chemical ORE exists as output
                leftovers
                    .entry(in_q.chemical.clone())
                    .and_modify(|q| {
                        let new_q = q.get() + in_q.value;
                        q.replace(new_q);
                    })
                    .or_insert(Cell::new(in_q.value));
            };
        });

        next.output.clone()
    }
}

fn main() {
    task1();
    task2();
}

/// 216477
fn task1() {
    let conversions: Result<Conversions, String> =
        std::fs::read_to_string("input").unwrap().trim().parse();

    let conversions = conversions.unwrap();
    let ore_cost = calc_ore_cost(&conversions, &mut HashMap::new());

    println!("Task 1 answer is {:?}", ore_cost);
}

/// Not 4_619_428 (too low)
fn task2() {
    let conversions: Result<Conversions, String> =
        std::fs::read_to_string("input").unwrap().trim().parse();
    let conversions = conversions.unwrap();
    let fuel = calc_fuel_output(&conversions, 1_000_000_000_000);

    println!("Task 2 answer is {:?}", fuel);
}

fn calc_fuel_output(conversions: &Conversions, available_ore: u64) -> u64 {
    let mut leftovers = HashMap::new();
    let fuel = (0..)
        .try_fold((0 as u64, 0 as u64), |(acc_ore_cost, acc_fuel), _| {
            let ore_cost = calc_ore_cost(&conversions, &mut leftovers);
            let acc_ore_cost = acc_ore_cost + ore_cost;

            if acc_fuel % 100_000 == 0 {
                dbg!(acc_ore_cost as f64 / available_ore as f64);
            }

            if acc_ore_cost > available_ore {
                Err(acc_fuel)
            } else {
                Ok((acc_ore_cost, acc_fuel + 1))
            }
        })
        .unwrap_err();
    fuel
}

fn calc_ore_cost(conversions: &Conversions, leftovers: &mut HashMap<Chemical, Cell<u64>>) -> u64 {
    let fuel_converter = conversions
        .find_conversion_with_output(&Chemical::new("FUEL"))
        .unwrap();
    let _ = conversions.convert(leftovers, &fuel_converter);
    let ore_cost = leftovers.remove(&Chemical::new("ORE"));

    ore_cost.unwrap().get()
}

// Parsing

impl FromStr for Conversions {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let conversions: Result<Vec<Conversion>, String> = s
            .trim()
            .lines()
            .map(|l| l.trim())
            .map(|l| l.parse::<Conversion>())
            .collect();
        let conversions = conversions?;

        Ok(Self { conversions })
    }
}

impl FromStr for Conversion {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let io: Vec<&str> = s.split("=>").collect();
        let input: Result<Vec<Quantity>, String> =
            io[0].split(",").map(|s| s.trim().parse()).collect();
        let input = input?;
        let output = io[1].trim().parse()?;

        Ok(Self { input, output })
    }
}

impl FromStr for Quantity {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.trim().split_whitespace();

        let value = split.next().ok_or("No amount".to_string());
        let value: Result<u64, ParseIntError> = value?.parse();
        let value = value.map_err(|e| e.to_string())?;

        let chemical = split.next().ok_or("No chemical".to_string());
        let chemical = chemical?.parse();
        let chemical = chemical?;

        Ok(Self { chemical, value })
    }
}

impl FromStr for Chemical {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            code: s.trim().to_string(),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example_task1_small() -> Result<(), String> {
        let conversions: Result<Conversions, String> = r#"
            9 ORE => 2 A
            8 ORE => 3 B
            7 ORE => 5 C
            3 A, 4 B => 1 AB
            5 B, 7 C => 1 BC
            4 C, 1 A => 1 CA
            2 AB, 3 BC, 4 CA => 1 FUEL
        "#
        .parse();
        assert_eq!(calc_ore_cost(&conversions?, &mut HashMap::new()), 165);
        Ok(())
    }

    #[test]
    fn example_task1_larger() -> Result<(), String> {
        let conversions: Result<Conversions, String> = r#"
            171 ORE => 8 CNZTR
            7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
            114 ORE => 4 BHXH
            14 VRPVC => 6 BMBT
            6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
            6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
            15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
            13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
            5 BMBT => 4 WPTQ
            189 ORE => 9 KTJDG
            1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
            12 VRPVC, 27 CNZTR => 2 XDBXC
            15 KTJDG, 12 BHXH => 5 XCVML
            3 BHXH, 2 VRPVC => 7 MZWV
            121 ORE => 7 VRPVC
            7 XCVML => 6 RJRHP
            5 BHXH, 4 VRPVC => 5 LTCX
        "#
        .parse();
        assert_eq!(calc_ore_cost(&conversions?, &mut HashMap::new()), 2210736);
        Ok(())
    }

    #[test]
    fn example_task2_small() -> Result<(), String> {
        let conversions: Result<Conversions, String> = r#"
            9 ORE => 2 A
            8 ORE => 3 B
            7 ORE => 5 C
            3 A, 4 B => 1 AB
            5 B, 7 C => 1 BC
            4 C, 1 A => 1 CA
            2 AB, 3 BC, 4 CA => 1 FUEL
        "#
        .parse();
        assert_eq!(
            calc_fuel_output(&conversions?, 1_000_000_000_000),
            82_892_753
        );
        Ok(())
    }
}
