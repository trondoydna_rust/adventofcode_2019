use crate::painter_robot::Colour;
use std::cell::Cell;
use std::time::Duration;

mod intcode {
    #[derive(Clone, Copy, Debug)]
    enum ParamMode {
        Position,
        Immediate,
        Relative,
    }

    #[derive(Clone, Copy, Debug)]
    struct Input {
        value: isize,
    }

    #[derive(Debug)]
    struct Output {
        location: usize,
    }

    #[derive(Debug)]
    enum Operation {
        Add(Input, Input, Output),
        Multiply(Input, Input, Output),
        Input(Output),
        Output(Input),
        JumpIfTrue(Input, Input),
        JumpIfFalse(Input, Input),
        LessThan(Input, Input, Output),
        Equals(Input, Input, Output),
        AdjustRelativeBase(Input),
        Terminate,
    }

    impl Operation {
        fn new(code: usize, run: &mut Program) -> Self {
            match code {
                1 => Operation::Add(run.input(1), run.input(2), run.output(3)),
                2 => Operation::Multiply(run.input(1), run.input(2), run.output(3)),
                3 => Operation::Input(run.output(1)),
                4 => Operation::Output(run.input(1)),
                5 => Operation::JumpIfTrue(run.input(1), run.input(2)),
                6 => Operation::JumpIfFalse(run.input(1), run.input(2)),
                7 => Operation::LessThan(run.input(1), run.input(2), run.output(3)),
                8 => Operation::Equals(run.input(1), run.input(2), run.output(3)),
                9 => Operation::AdjustRelativeBase(run.input(1)),
                99 => Operation::Terminate,
                n => panic!("Unknown opcode {}", n),
            }
        }

        fn execute(&mut self, run: &mut Program) -> Result<Option<isize>, ()> {
            match self {
                Operation::Add(i1, i2, o) => {
                    run.write_to(o, i1.value + i2.value);

                    Ok(None)
                }
                Operation::Multiply(i1, i2, o) => {
                    run.write_to(o, i1.value * i2.value);

                    Ok(None)
                }
                Operation::Input(o) => {
                    run.write_to(o, run.input.expect("Expected input but there was none"));

                    Ok(None)
                }
                Operation::Output(i) => Ok(Some(i.value)),
                Operation::JumpIfTrue(_, _) | Operation::JumpIfFalse(_, _) => Ok(None),
                Operation::LessThan(i1, i2, o) => {
                    if i1.value < i2.value {
                        run.write_to(o, 1);
                    } else {
                        run.write_to(o, 0);
                    }
                    Ok(None)
                }
                Operation::Equals(i1, i2, o) => {
                    if i1.value == i2.value {
                        run.write_to(o, 1);
                    } else {
                        run.write_to(o, 0);
                    }
                    Ok(None)
                }
                Operation::AdjustRelativeBase(i) => {
                    run.relative_base += i.value;
                    Ok(None)
                }
                Operation::Terminate => Err(()),
            }
        }

        fn next_instruction_index(&self, current: usize) -> usize {
            match self {
                Operation::Terminate => 0,
                Operation::Input(_) | Operation::Output(_) | Operation::AdjustRelativeBase(_) => {
                    current + 2
                }
                Operation::Add(_, _, _)
                | Operation::Multiply(_, _, _)
                | Operation::LessThan(_, _, _)
                | Operation::Equals(_, _, _) => current + 4,
                Operation::JumpIfTrue(i1, i2) => {
                    if i1.value != 0 {
                        i2.value as usize
                    } else {
                        current + 3
                    }
                }
                Operation::JumpIfFalse(i1, i2) => {
                    if i1.value == 0 {
                        i2.value as usize
                    } else {
                        current + 3
                    }
                }
            }
        }
    }

    #[derive(Clone)]
    pub struct Program {
        intcodes: Vec<isize>,
        input: Option<isize>,
        instruction_index: usize,
        relative_base: isize,
    }

    impl Program {
        pub fn new(intcodes: Vec<isize>) -> Self {
            Self {
                intcodes,
                input: None,
                instruction_index: 0,
                relative_base: 0,
            }
        }

        pub fn run_to_completion(&mut self, input: isize) -> Vec<isize> {
            self.set_input(input);
            self.flatten().collect()
        }

        pub fn set_input(&mut self, input: isize) {
            self.input = Some(input);
        }

        fn input(&mut self, offset: usize) -> Input {
            let mode = self.param_mode(offset);
            let i = self.instruction_index + offset;
            let val = self.read_from(i);

            let value = match mode {
                ParamMode::Position => self.read_from(val as usize),
                ParamMode::Immediate => val,
                ParamMode::Relative => self.read_from((val + self.relative_base) as usize),
            };

            Input { value }
        }

        fn read_from(&mut self, i: usize) -> isize {
            if i > self.intcodes.len() {
                self.intcodes.resize(i + 1, 0);
                0
            } else {
                self.intcodes[i]
            }
        }

        fn output(&self, offset: usize) -> Output {
            let mode = self.param_mode(offset);

            let location = self.intcodes[self.instruction_index + offset] as usize;
            let location = match mode {
                ParamMode::Position => location,
                ParamMode::Relative => (location as isize + self.relative_base) as usize,
                ParamMode::Immediate => panic!("Cannot write with mode immediate"),
            };

            Output { location }
        }

        fn write_to(&mut self, output: &Output, value: isize) {
            let i = output.location;
            if i >= self.intcodes.len() {
                self.intcodes.resize(i + 1, 0);
            }
            self.intcodes[i] = value;
        }

        fn param_mode(&self, offset: usize) -> ParamMode {
            let mut opcode = self.intcodes[self.instruction_index] as usize;

            let mut mode = ParamMode::Position;

            if opcode > 20_000 {
                mode = ParamMode::Relative;
                opcode -= 20_000;
            }
            if opcode > 10_000 {
                mode = ParamMode::Immediate;
                opcode -= 10_000;
            }
            if offset == 3 {
                return mode;
            }

            let mut mode = ParamMode::Position;
            if opcode > 2_000 {
                mode = ParamMode::Relative;
                opcode -= 2_000;
            }
            if opcode > 1_000 {
                mode = ParamMode::Immediate;
                opcode -= 1_000;
            }
            if offset == 2 {
                return mode;
            }

            if opcode > 200 {
                ParamMode::Relative
            } else if opcode > 100 {
                ParamMode::Immediate
            } else {
                ParamMode::Position
            }
        }

        fn opcode(&self) -> usize {
            self.intcodes[self.instruction_index] as usize % 100
        }
    }

    impl<'a> Iterator for Program {
        type Item = Option<isize>;

        fn next(&mut self) -> Option<Self::Item> {
            let opcode = self.opcode();

            let mut operation = Operation::new(opcode, self);

            let next_instruction_index = operation.next_instruction_index(self.instruction_index);
            let res = operation.execute(self);

            self.instruction_index = next_instruction_index;

            match res {
                Ok(o) => Some(o),
                Err(()) => None,
            }
        }
    }
}

mod painter_robot {
    use std::collections::{HashMap, HashSet};
    use std::fmt::{Display, Error, Formatter};

    #[derive(Debug, Clone, Copy, Eq, PartialEq)]
    pub enum Colour {
        Black,
        White,
    }

    impl Colour {
        pub fn from_code(code: &isize) -> Self {
            match code {
                0 => Colour::Black,
                1 => Colour::White,
                n => panic!("Unknown colour code {}", n),
            }
        }
        pub fn to_code(&self) -> isize {
            match self {
                Colour::Black => 0,
                Colour::White => 1,
            }
        }
    }

    impl Display for Colour {
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
            write!(
                f,
                "{}",
                match self {
                    Colour::Black => "■",
                    Colour::White => "□",
                }
            )
        }
    }

    #[derive(Debug, Clone, Copy, Eq, PartialEq)]
    pub enum Direction {
        Up,
        Right,
        Down,
        Left,
    }

    impl Direction {
        pub fn from_code(code: &isize) -> Self {
            match code {
                0 => Direction::Left,
                1 => Direction::Right,
                n => panic!("Unknown colour code {}", n),
            }
        }
    }

    #[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
    pub struct Location {
        x: isize,
        y: isize,
    }

    impl Location {
        fn new() -> Self {
            Location { x: 0, y: 0 }
        }

        fn absolute_move(&self, direction: Direction) -> Self {
            match direction {
                Direction::Up => Location {
                    x: self.x,
                    y: self.y + 1,
                },
                Direction::Right => Location {
                    x: self.x + 1,
                    y: self.y,
                },
                Direction::Down => Location {
                    x: self.x,
                    y: self.y - 1,
                },
                Direction::Left => Location {
                    x: self.x - 1,
                    y: self.y,
                },
            }
        }
    }

    struct Robot {
        location: Location,
        direction: Direction,
    }

    impl Robot {
        fn relative_move(&mut self, direction: Direction) {
            dbg!(&self.location, direction);

            self.direction = match self.direction {
                Direction::Up => match direction {
                    Direction::Left => Direction::Left,
                    Direction::Right => Direction::Right,
                    d => panic!("Cannot turn {:?}", d),
                },
                Direction::Right => match direction {
                    Direction::Left => Direction::Up,
                    Direction::Right => Direction::Down,
                    d => panic!("Cannot turn {:?}", d),
                },
                Direction::Down => match direction {
                    Direction::Left => Direction::Right,
                    Direction::Right => Direction::Left,
                    d => panic!("Cannot turn {:?}", d),
                },
                Direction::Left => match direction {
                    Direction::Left => Direction::Down,
                    Direction::Right => Direction::Up,
                    d => panic!("Cannot turn {:?}", d),
                },
            };
            self.location = self.location.absolute_move(self.direction);
        }
    }

    pub struct State {
        grid: HashMap<Location, Colour>,
        robot: Robot,
        changed_tiles: HashSet<Location>,
    }

    impl State {
        pub fn new() -> Self {
            Self {
                grid: HashMap::new(),
                robot: Robot {
                    location: Location::new(),
                    direction: Direction::Up,
                },
                changed_tiles: HashSet::new(),
            }
        }

        pub fn current_colour(&self) -> Colour {
            *self
                .grid
                .get(&self.robot.location)
                .unwrap_or(&Colour::Black)
        }

        pub fn set_white_start(&mut self) {
            self.grid.insert(Location::new(), Colour::White);
        }

        pub fn paint(&mut self, colour: Colour) {
            self.grid.insert(self.robot.location, colour);
            self.changed_tiles.insert(self.robot.location);
        }

        pub fn relative_move(&mut self, direction: Direction) {
            self.robot.relative_move(direction);
        }

        pub fn changed_tiles(&self) -> usize {
            self.changed_tiles.len()
        }
    }

    impl Display for State {
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
            let min_x = self
                .grid
                .keys()
                .min_by(|l1, l2| l1.x.cmp(&l2.x))
                .map(|l| l.x)
                .unwrap_or(0);
            let max_x = self
                .grid
                .keys()
                .max_by(|l1, l2| l1.x.cmp(&l2.x))
                .map(|l| l.x)
                .unwrap_or(0);
            let min_y = self
                .grid
                .keys()
                .min_by(|l1, l2| l1.y.cmp(&l2.y))
                .map(|l| l.y)
                .unwrap_or(0);
            let max_y = self
                .grid
                .keys()
                .max_by(|l1, l2| l1.y.cmp(&l2.y))
                .map(|l| l.y)
                .unwrap_or(0);

            dbg!(&min_x);
            dbg!(&max_x);
            dbg!(&min_y);
            dbg!(&max_y);

            for y in (min_y..=max_y).rev() {
                for x in min_x..=max_x {
                    let l = Location {x, y};
                    write!(f, "{}", self.grid.get(&l).unwrap_or(&Colour::Black))?;
                }
                writeln!(f)?;
            }

            Ok(())
        }
    }
}

fn main() {
    //task1();
    task2();
}

/// 2184
fn task1() {
    let state = run(Colour::Black);

    println!("Task 1 answer is {:?}", state.changed_tiles());
}

/// AHCHZEPK
fn task2() {
    let state = run(Colour::White);

    println!("Task 2 answer is");
    println!("{}", state);
}

fn run(initial: Colour) -> painter_robot::State {
    use painter_robot::*;

    let intcodes: Vec<isize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<isize>().unwrap())
        .collect();

    let mut program = intcode::Program::new(intcodes);
    program.set_input(initial.to_code());

    let mut state = State::new();
    let mut flag = true;

    while let Some(output) = program.next() {
        if let Some(instruction) = output {
            dbg!(output);
            if flag {
                state.paint(dbg!(Colour::from_code(&instruction)));
            } else {
                state.relative_move(dbg!(Direction::from_code(&instruction)));
                program.set_input(dbg!(state.current_colour()).to_code());
                dbg!();
            }
            flag = !flag;
        }
    }

    state
}

#[cfg(test)]
mod tests {
    use super::intcode::*;
    use super::*;

    #[test]
    fn test_in_out() {
        assert_eq!(
            Program::new(vec![3, 0, 4, 0, 99]).run_to_completion(1),
            vec![1]
        );
    }

    #[test]
    fn test_immediate() {
        assert_eq!(
            Program::new(vec![1002, 4, 3, 4, 33]).run_to_completion(1),
            vec![]
        );
    }

    #[test]
    fn test_immediate_2() {
        assert_eq!(
            Program::new(vec![1101, 100, -1, 4, 0]).run_to_completion(1),
            vec![]
        );
    }

    #[test]
    fn is_equal_to() {
        assert_eq!(
            Program::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(8),
            vec![1]
        );
        assert_eq!(
            Program::new(vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(88),
            vec![0]
        );

        assert_eq!(
            Program::new(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99]).run_to_completion(8),
            vec![1]
        );
        assert_eq!(
            Program::new(vec![3, 3, 1108, -1, 8, 3, 4, 3, 99]).run_to_completion(88),
            vec![0]
        );
    }

    #[test]
    fn is_less_than() {
        assert_eq!(
            Program::new(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(0),
            vec![1]
        );
        assert_eq!(
            Program::new(vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8]).run_to_completion(8),
            vec![0]
        );

        assert_eq!(
            Program::new(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99]).run_to_completion(0),
            vec![1]
        );
        assert_eq!(
            Program::new(vec![3, 3, 1107, -1, 8, 3, 4, 3, 99]).run_to_completion(8),
            vec![0]
        );
    }

    #[test]
    fn looping() {
        assert_eq!(
            Program::new(vec![
                3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9
            ])
            .run_to_completion(0),
            vec![0]
        );
        assert_eq!(
            Program::new(vec![
                3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9
            ])
            .run_to_completion(111),
            vec![1]
        );

        assert_eq!(
            Program::new(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1])
                .run_to_completion(0),
            vec![0]
        );
        assert_eq!(
            Program::new(vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1])
                .run_to_completion(111),
            vec![1]
        );
    }

    #[test]
    fn example_day5() {
        let mut program = Program::new(vec![
            3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0,
            0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4,
            20, 1105, 1, 46, 98, 99,
        ]);
        assert_eq!(program.clone().run_to_completion(1), vec![999]);

        assert_eq!(program.clone().run_to_completion(8), vec![1000]);

        assert_eq!(program.run_to_completion(17), vec![1001]);
    }

    #[test]
    fn copy_self() {
        assert_eq!(
            Program::new(vec![
                109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99
            ])
            .run_to_completion(111),
            vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
        );
    }

    #[test]
    fn output_middle_number() {
        assert_eq!(
            Program::new(vec![104, 1125899906842624, 99]).run_to_completion(111),
            vec![1125899906842624]
        );
    }
}
