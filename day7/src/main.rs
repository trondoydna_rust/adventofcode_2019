use crate::intcode::Program;
use std::collections::HashSet;

mod intcode {
    #[derive(Clone, Copy, Debug)]
    enum ParamMode {
        Position,
        Immediate,
        Relative,
    }

    #[derive(Clone, Copy, Debug)]
    struct Input {
        value: isize,
    }

    #[derive(Debug)]
    struct Output {
        location: usize,
    }

    #[derive(Debug)]
    enum Operation {
        Add(Input, Input, Output),
        Multiply(Input, Input, Output),
        Input(Output),
        Output(Input),
        JumpIfTrue(Input, Input),
        JumpIfFalse(Input, Input),
        LessThan(Input, Input, Output),
        Equals(Input, Input, Output),
        AdjustRelativeBase(Input),
        Terminate,
    }

    impl Operation {
        fn new(code: usize, program: &mut Program) -> Self {
            match code {
                1 => Operation::Add(program.input(1), program.input(2), program.output(3)),
                2 => Operation::Multiply(program.input(1), program.input(2), program.output(3)),
                3 => Operation::Input(program.output(1)),
                4 => Operation::Output(program.input(1)),
                5 => Operation::JumpIfTrue(program.input(1), program.input(2)),
                6 => Operation::JumpIfFalse(program.input(1), program.input(2)),
                7 => Operation::LessThan(program.input(1), program.input(2), program.output(3)),
                8 => Operation::Equals(program.input(1), program.input(2), program.output(3)),
                9 => Operation::AdjustRelativeBase(program.input(1)),
                99 => Operation::Terminate,
                n => panic!("Unknown opcode {}", n),
            }
        }

        fn execute(&mut self, run: &mut Program) -> Result<Option<isize>, ()> {
            match self {
                Operation::Add(i1, i2, o) => {
                    run.write_to(o, i1.value + i2.value);

                    Ok(None)
                }
                Operation::Multiply(i1, i2, o) => {
                    run.write_to(o, i1.value * i2.value);

                    Ok(None)
                }
                Operation::Input(o) => {
                    run.write_to(o, dbg!(run.input[run.input_index]));
                    run.input_index += 1;

                    Ok(None)
                }
                Operation::Output(i) => Ok(Some(i.value)),
                Operation::JumpIfTrue(_, _) | Operation::JumpIfFalse(_, _) => Ok(None),
                Operation::LessThan(i1, i2, o) => {
                    if i1.value < i2.value {
                        run.write_to(o, 1);
                    } else {
                        run.write_to(o, 0);
                    }
                    Ok(None)
                }
                Operation::Equals(i1, i2, o) => {
                    if i1.value == i2.value {
                        run.write_to(o, 1);
                    } else {
                        run.write_to(o, 0);
                    }
                    Ok(None)
                }
                Operation::AdjustRelativeBase(i) => {
                    run.relative_base += i.value;
                    Ok(None)
                }
                Operation::Terminate => Err(()),
            }
        }

        fn next_instruction_index(&self, current: usize) -> usize {
            match self {
                Operation::Terminate => 0,
                Operation::Input(_) | Operation::Output(_) | Operation::AdjustRelativeBase(_) => {
                    current + 2
                }
                Operation::Add(_, _, _)
                | Operation::Multiply(_, _, _)
                | Operation::LessThan(_, _, _)
                | Operation::Equals(_, _, _) => current + 4,
                Operation::JumpIfTrue(i1, i2) => {
                    if i1.value != 0 {
                        i2.value as usize
                    } else {
                        current + 3
                    }
                }
                Operation::JumpIfFalse(i1, i2) => {
                    if i1.value == 0 {
                        i2.value as usize
                    } else {
                        current + 3
                    }
                }
            }
        }
    }

    #[derive(Clone, Debug)]
    pub struct Program {
        intcodes: Vec<isize>,
        input: Vec<isize>,
        input_index: usize,
        instruction_index: usize,
        relative_base: isize,
    }

    impl Program {
        pub fn new(intcodes: Vec<isize>) -> Self {
            Self {
                intcodes,
                input: Vec::new(),
                input_index: 0,
                instruction_index: 0,
                relative_base: 0,
            }
        }

        pub fn run_to_completion(&mut self, input: Vec<isize>) -> Vec<isize> {
            self.set_input(input);
            self.flatten().collect()
        }

        pub fn set_input(&mut self, input: Vec<isize>) {
            self.input = input;
        }

        pub fn add_input(&mut self, input: isize) {
            self.input.push(input);
        }

        fn input(&mut self, offset: usize) -> Input {
            let mode = self.param_mode(offset);
            let i = self.instruction_index + offset;
            let val = self.read_from(i);

            let value = match mode {
                ParamMode::Position => self.read_from(val as usize),
                ParamMode::Immediate => val,
                ParamMode::Relative => self.read_from((val + self.relative_base) as usize),
            };

            Input { value }
        }

        fn read_from(&mut self, i: usize) -> isize {
            if i > self.intcodes.len() {
                self.intcodes.resize(i + 1, 0);
                0
            } else {
                self.intcodes[i]
            }
        }

        fn output(&self, offset: usize) -> Output {
            let mode = self.param_mode(offset);

            let location = self.intcodes[self.instruction_index + offset] as usize;
            let location = match mode {
                ParamMode::Position => location,
                ParamMode::Relative => (location as isize + self.relative_base) as usize,
                ParamMode::Immediate => panic!("Cannot write with mode immediate"),
            };

            Output { location }
        }

        fn write_to(&mut self, output: &Output, value: isize) {
            let i = output.location;
            if i >= self.intcodes.len() {
                self.intcodes.resize(i + 1, 0);
            }
            self.intcodes[i] = value;
        }

        fn param_mode(&self, offset: usize) -> ParamMode {
            let mut opcode = self.intcodes[self.instruction_index] as usize;

            let mut mode = ParamMode::Position;

            if opcode > 20_000 {
                mode = ParamMode::Relative;
                opcode -= 20_000;
            }
            if opcode > 10_000 {
                mode = ParamMode::Immediate;
                opcode -= 10_000;
            }
            if offset == 3 {
                return mode;
            }

            let mut mode = ParamMode::Position;
            if opcode > 2_000 {
                mode = ParamMode::Relative;
                opcode -= 2_000;
            }
            if opcode > 1_000 {
                mode = ParamMode::Immediate;
                opcode -= 1_000;
            }
            if offset == 2 {
                return mode;
            }

            if opcode > 200 {
                ParamMode::Relative
            } else if opcode > 100 {
                ParamMode::Immediate
            } else {
                ParamMode::Position
            }
        }

        fn opcode(&self) -> usize {
            self.intcodes[self.instruction_index] as usize % 100
        }
    }

    impl<'a> Iterator for Program {
        type Item = Option<isize>;

        fn next(&mut self) -> Option<Self::Item> {
            let opcode = self.opcode();

            let mut operation = Operation::new(opcode, self);

            let next_instruction_index = operation.next_instruction_index(self.instruction_index);
            let res = operation.execute(self);

            self.instruction_index = next_instruction_index;

            match res {
                Ok(o) => Some(o),
                Err(()) => None,
            }
        }
    }
}

fn main() {
    task1();
    task2();
}

/// 206580
fn task1() {
    let program: Vec<isize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<isize>().unwrap())
        .collect();

    let program = Program::new(program);

    let mut outputs = Vec::new();
    for i0 in 0..=4 {
        for i1 in 0..=4 {
            for i2 in 0..=4 {
                for i3 in 0..=4 {
                    for i4 in 0..=4 {
                        let mut set = HashSet::new();
                        set.insert(i0);
                        set.insert(i1);
                        set.insert(i2);
                        set.insert(i3);
                        set.insert(i4);

                        if set.len() == 5 {
                            let settings = vec![i0, i1, i2, i3, i4];

                            println!("{:?}", &settings);

                            let output = run_with_settings(program.clone(), &settings);

                            println!("{}", output);

                            outputs.push(output);
                        }
                    }
                }
            }
        }
    }

    let result = outputs.iter().max();

    println!("Task 1 answer is {:?}", result);
}

fn run_with_settings(program: Program, settings: &Vec<isize>) -> isize {
    settings.iter().fold(0, |previous_output, &next_setting| {
        let mut program = program.clone();
        program.set_input(vec![next_setting, previous_output]);

        loop {
            let next = program.next();

            dbg!(next_setting, &next);

            match next {
                Some(Some(out)) => break out,
                Some(None) => continue,
                None => panic!("No more output"),
            }
        }
    })
}

/// 2299406
fn task2() {
    let program: Vec<isize> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .split(",")
        .map(|c| c.parse::<isize>().unwrap())
        .collect();

    let mut amps = Vec::new();

    for i0 in 5..=9 as isize {
        for i1 in 5..=9 as isize {
            for i2 in 5..=9 as isize {
                for i3 in 5..=9 as isize {
                    for i4 in 5..=9 as isize {
                        let mut set = HashSet::new();
                        set.insert(i0);
                        set.insert(i1);
                        set.insert(i2);
                        set.insert(i3);
                        set.insert(i4);

                        if set.len() == 5 {
                            amps.push(
                                [
                                    (Program::new(program.clone()), i0),
                                    (Program::new(program.clone()), i1),
                                    (Program::new(program.clone()), i2),
                                    (Program::new(program.clone()), i3),
                                    (Program::new(program.clone()), i4),
                                ]
                                .to_vec(),
                            );
                        }
                    }
                }
            }
        }
    }

    let res = amps.into_iter().map(loop_run).max().expect("No output");

    println!("Task 2 answer is {:?}", res);
}
fn loop_run(pairs: Vec<(Program, isize)>) -> isize {
    let mut last_output = 0;

    let mut programs = pairs
        .into_iter()
        .map(|(mut program, setting)| {
            program.set_input(vec![setting]);
            program
        })
        .collect::<Vec<Program>>();

    'outer: for i in std::iter::repeat(0..=4).flatten() {
        let program = &mut programs[i];
        program.add_input(last_output);

        last_output = loop {
            let next = program.next();

            match next {
                Some(Some(out)) => break dbg!(out),
                Some(None) => continue,
                None => break 'outer,
            }
        }
    }

    last_output
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn examples_task1() {
        let program = Program::new(vec![
            3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0,
        ]);
        let settings = vec![4, 3, 2, 1, 0];

        assert_eq!(run_with_settings(program, &settings), 43210);

        let program = Program::new(vec![
            3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23,
            99, 0, 0,
        ]);
        let settings = vec![0, 1, 2, 3, 4];

        assert_eq!(run_with_settings(program, &settings), 54321);

        let program = Program::new(vec![
            3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7, 33, 1,
            33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0,
        ]);
        let settings = vec![1, 0, 4, 3, 2];

        assert_eq!(run_with_settings(program, &settings), 65210);
    }

    #[test]
    fn examples_task2() {
        let program = Program::new(vec![
            3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001, 28, -1,
            28, 1005, 28, 6, 99, 0, 0, 5,
        ]);

        assert_eq!(
            loop_run(vec![
                (program.clone(), 9),
                (program.clone(), 8),
                (program.clone(), 7),
                (program.clone(), 6),
                (program.clone(), 5)
            ]),
            139629729
        );

        let program = Program::new(vec![
            3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26, 1001, 54,
            -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4,
            53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10,
        ]);
        let settings = vec![9, 7, 8, 5, 6];

        assert_eq!(
            loop_run(vec![
                (program.clone(), 9),
                (program.clone(), 7),
                (program.clone(), 8),
                (program.clone(), 5),
                (program.clone(), 6)
            ]),
            18216
        );
    }
}
